(* higher order *********************************************************)
let rec mymap al fn=match al with
    | [] -> []
    | h::t ->(fn h) :: (mymap t fn);;
let add x y=x+y;;
let f=add 6;;
Printf.printf "%d\n" (f 5);;
Printf.printf "%d\n" ((add 6) 5);;
let a= List.hd (mymap  [10;20;30] f);;
Printf.printf "%d\n" a;;
let b= List.hd (mymap [10;20;30] (fun x -> x*2));;
Printf.printf "%d\n" b;;
let c= List.hd (mymap [10;20;30] ( ( * ) 2 ) );;
Printf.printf "%d\n" c;;
