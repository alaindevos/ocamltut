module IntPairs = struct
  type t = int * int

  let compare (x0, y0) (x1, y1) =
    match Stdlib.compare x0 x1 with 0 -> Stdlib.compare y0 y1 | c -> c
end

module PairsMap = Map.Make (IntPairs)

let m = PairsMap.(empty |> add (0, 1) "hello" |> add (1, 0) "world") ;;
let s= PairsMap.find (1,0) m in 
print_string s;;
