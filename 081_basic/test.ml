(* BASIC *****************************)
let x=1 in
let y=1 in
print_int (x+y);;
let f= (fun x -> x+2);;
print_int (f 5);;
let plus = ( + ) ;;
print_int (plus 3 5);;
let a=[1;2];;
let b=3::a;;
let c=[3;4]@b;;
let a=ref 0;;
a:=1;;
print_int !a;;
let swaparray a x y=
    let t=a.(x) in
    a.(x) <- a.(y);
    a.(y) <- t;;
let b=[|3;7|];;
swaparray b 0 1;;
print_int (b.(0));;
if 1=1 then
    begin
        print_int 1;
        print_int 2
    end
else
    print_int 3;;
let f=1.0 *. 2.0;;
