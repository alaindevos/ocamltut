(* Loop *********************************************************)

let rec sum1 n =
  if n <= 0 then 1
  else n + sum1 (n-1) ;;

let sum2 n =
  let rec loop i accum =
    if i > n then accum
    else loop (i + 1) (accum + i)
  in loop 1 1 ;;

let sum3 n =
  let result = ref 1 in
  for i = 1 to n do
    result := !result + i
  done;
  !result ;;

let sum4 n =
    let nref = ref n in
    let p = ref 1 in
        while !nref > 0 do
            p := !p + !nref;
            nref := !nref - 1;
        done;
    !p ;;

Printf.printf "%d\n" (sum1(200000));;
Printf.printf "%d\n" (sum2(200000));;
Printf.printf "%d\n" (sum3(200000));;
Printf.printf "%d\n" (sum4(200000));;

