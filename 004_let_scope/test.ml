(* let scope ********************************************************************)
let a=0 in a;;
let b=1 in 2*b;;
let c=3 in
    (let d=4 in c+d);;
let e=5 in
    let f="6" in 
    let ()= print_int (int_of_string f) in 
    print_int e





