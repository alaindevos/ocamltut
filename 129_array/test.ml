let _ =
    let a = [| 1;2;3;4|] in
    print_int  a.(0);
    a.(1) <- 5;
    let x = Array.make 10 1 in
    print_int (Array.length x);
    ()
