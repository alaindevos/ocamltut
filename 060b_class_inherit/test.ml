(* Class inherit ******************************)
class myclass (init:int)=
    object(self)
    val mutable i:int=init
    method inc () = i<-i+1
    method geti ():int =i
end

class myclass2 (init:int)=
    object(self)
        inherit myclass init
    end

let _=
    let x=new myclass2 2 in
    x#inc ();
    print_int (x#geti ());
    ()
