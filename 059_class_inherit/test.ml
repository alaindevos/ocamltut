(* Class inherit ****************************************)
class counter = object 
    val mutable v=0
    method inc =
        v <-v+1
    method printv=
        v
end;;

class counter2=object
    inherit counter
    method private dec =
        v <- v-1
end;;

let c:counter=new counter;;
c#inc;;
print_int c#printv;;

