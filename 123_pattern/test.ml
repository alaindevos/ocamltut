let _ =
    let rec fib1 i = 
        match i with
        | 0 -> 0
        | 1 -> 1
        | j -> fib1(j-2)+fib1(j-1) in
    print_int (fib1 5);
    let rec fib2 i =
        match i with
        | (0|1) -> i
        | i -> fib2(i-2)+fib2(i-1) in
    print_int (fib2 5);
    let rec fib3 i =
        match i with
        | i when  i<2 -> i
        | i -> fib3(i-2)+fib3(i-1) in
    print_int (fib3 5);
    let is_upper x =
        match x with
        | 'A'|'B' -> 1 
        | c -> 0 in
    print_int (is_upper 'B');

        

