(* manually unfold take, sequence of integer fibonaci numbers *)
(* Nil, empty not used here as the sequence has no end *)
type intseq = Cons of (int * int) * (unit -> intseq)

let _ =
  let rec afib (ab : int * int) : intseq =
    match ab with a, b -> Cons (ab, fun () -> afib (b, a + b))
  in
  let fibs : intseq = afib (0, 1) in
  let head (Cons (h, _)) : int * int = h in
  let tail (Cons (_, tf)) : intseq = tf () in
  (*force-evaluate the tail*)
  let rec mytake (num : int) (seq : intseq) : 'a list =
    if num = 0 then [] else head seq :: mytake (num - 1) (tail seq)
  in
  let take10 : (int * int) list = mytake 10 fibs in
  let rec fgetnth (n : int) (lst : 'a list) : int * int =
    if n = 1 then List.hd lst else fgetnth (n - 1) (List.tl lst)
  in
  let x : int = snd (fgetnth 10 take10) in
  print_int x
