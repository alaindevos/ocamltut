(* mutable counter *********************************************)
let ()=
    let x=ref 0 in
    let incx () = 
        let ()= x:=!x+1 in 
        () in 
    let ()=incx () in
    let ()=incx () in
    let ()=incx () in
    let ()=print_int !x in
    ()
