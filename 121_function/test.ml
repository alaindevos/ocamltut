let _ =
    (*function*)
    let inc1 = fun i -> i +1 in
    print_int (inc1 3);
    let sum1 = fun i j -> i+j in
    let sum2 =( fun i -> (fun j -> i+j )) in
    print_int (sum1 2 3);
    print_int (sum2 2 3);
    let inc2 = sum2 1 in
    print_int (inc2 3);
    let sum3 i j = i + j in
    print_int (sum3 2 3);
    let dx=1e-10 in
    let f' f=fun x -> ( f (x +. dx)-. f(x)/. dx) in
    let d = f' (Float.pow 3.0) 10.0 in
    print_float d;
    (*labelled arguments*)
    let f ~x:i ~y:j = i -j in
    print_int ( f ~y:2 ~x:1);
    (*shorthand*)
    let f ~x ~y = x-y in
    let x = 1 in
    let y = 2 in
    print_int (f ~x ~y);




