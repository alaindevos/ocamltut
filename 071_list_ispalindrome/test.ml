(* Check if list is palindrome ********************)
type stringlist=string list 
let ispalindrome (lst:stringlist):bool =
    let revlst=List.rev lst in 
    lst=revlst;;
let b:bool=ispalindrome ["a";"b";"c";"c";"b";"a"] ;;
Printf.printf "%B" b;;



