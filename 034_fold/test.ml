(* fold *********************************************************)
let rec foldleft afun acc alist = match alist with
    | [] -> acc
    | h::t -> foldleft afun (afun acc h) t
let res=foldleft (fun x y -> x + y) 0 [1;2;3];;
print_int res;
