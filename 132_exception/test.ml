exception Empty
let _ = 
    let split x =
        match x  with
        | [] -> raise Empty
        | h::t -> (h,t) in
    let rec mymap f l =
        try
            let (h,t)=split l in
            (f h):: (mymap f t) 
        with
            Empty -> [] in
    let myfun = fun i  ->  i+1 in 
    let  _ = mymap myfun [3;5;7] in 
()
