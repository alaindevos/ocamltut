(* for small lists *, string-int assocation list *****************************)
type stringint=string * int
type stringintlist=stringint list 
type intoption=int option
let _=
    let sil:stringintlist= [("Alain",21);("Eddy",40)] in
    (* warning this allows two same entries *) 
    let myinsert k v lst = (k,v)::lst in
    let rec mylookup (k:string) (lst:stringintlist): intoption =match lst with
        | [] -> None 
        | h::t -> 
            let (k2,v2)=h in 
            if k=k2 then 
                Some v2
            else
                mylookup k2 t in
    let sil2=myinsert "Frans" 3 sil in
    let _=mylookup "Alain" sil2 in
    (*find*)
    let _=List.assoc "Alain" sil2 in
    ()



