(* Infinite lazy fibonacci sequence *********************************)
type 'a seq = Cons of 'a * (unit -> 'a seq) | Nil
type intseq = int seq
type 'a lazyseq = Cons of 'a * 'a lazyseq Lazy.t
type lazyintseq = int lazyseq

let _ =
  let myval : int = 42 in
  let print_int_nl d =
    print_int d;
    print_newline ()
  in

  (*infinite sequence*)
  let rec fibseqfunction (a : int) (b : int) : intseq =
    Cons (a + b, fun () -> fibseqfunction b (a + b))
  in
  let fibseq : intseq = fibseqfunction 1 1 in
  let rec take (n : int) (s : intseq) : int list =
    if n <> 0 then
      match s with Cons (hd, tl) -> hd :: take (n - 1) (tl ()) | Nil -> []
    else []
  in
  let tail2 (n : int) (s : intseq) : int =
    take (n - 2) s |> List.rev |> List.hd
  in
  let r = tail2 myval fibseq in
  let _ = print_int_nl r in

  (*lazy evaluation*)
  let fib42lazy = lazy (tail2 myval fibseq) in
  let r = Lazy.force fib42lazy in
  let _ = print_int_nl r in

  (*lazy sequence*)
  let rec lazyfibseqfunction (a : int) (b : int) : lazyintseq =
    Cons (a + b, lazy (lazyfibseqfunction b (a + b)))
  in
  let lazyfibseq : lazyintseq = lazyfibseqfunction 1 1 in
  let rec lazytake (n : int) (s : lazyintseq) : int list =
    if n <> 0 then
      match s with Cons (hd, tl) -> hd :: lazytake (n - 1) (Lazy.force tl)
    else []
  in
  let lazytail2 (n : int) (s : lazyintseq) : int =
    lazytake (n - 2) s |> List.rev |> List.hd
  in
  let r = lazytail2 myval lazyfibseq in
  let _ = print_int_nl r in
  ()
