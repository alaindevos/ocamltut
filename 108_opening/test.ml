module A= struct
    type nil = NIL 
    let idem:(nil->nil)= fun x -> x
    let f1:(unit -> unit)= fun() ->  Printf.printf "A%!"
end 


module B= struct
    (* f2 get evaluated first and only *once*)
    let f2:unit = Printf.printf "B%!"
end

let (_:unit)=
    let _:A.nil=A.NIL in
    let _ = A.idem(A.NIL) in 
    let _ = A.idem(NIL) in   (* shorthand *) 
    let _ = A.NIL |> A.idem in 
    let _ = A.(NIL |> idem) in (*shorthand*) 
    (*Local open *)
    let open A in 
        let _= idem(NIL) in 
        let _= NIL |> idem in 

    let ()= A.f1 () in 
    let ()= B.f2 in (* does nothing *)
    () 

