(* Check prime *************************************)
let isprime (x:int):bool =
    let (p:bool ref)=ref true in 
    let () = 
        for y= 2 to (x-1) do 
            if (x mod y)=0 then
                p:=false
            else
                ()
        done in 
    !p;;

Printf.printf "%B\n" (isprime 17);;
Printf.printf "%B\n" (isprime 9);;

