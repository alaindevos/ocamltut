(* Loop recursive **************************************)
let ()=
    let rec myloop i= 
        if (i<>4) && (i<10) then
            let ()=Printf.printf "%d\n" i in 
            myloop (i+1)
        else
            () in 
    myloop 0

