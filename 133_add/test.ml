type operand=
    | MyPlus
    | MyMin

type expr =
    | Leaf of int 
    | Node of node 
and
 node={myop : operand ; expr1 : expr ; expr2 : expr} 
let _ =

    let rec myeval  = fun x ->
        match x with
        | Leaf l -> l
        | Node n -> if (n.myop = MyPlus) 
        then
            (myeval n.expr1) +(myeval n.expr2)
        else
            -1 in 
    let  myexpr1 = Leaf 1 in
    let  myexpr2 = Node { myop = MyPlus ; expr1 = Leaf 2 ; expr2 = Leaf 3} in
    let  x = myeval myexpr1 in
    let  y = myeval myexpr2 in
    (Printf.printf "%d" x);
    (Printf.printf "%d" y);
    ()

