type base = G | C | A | T 
type dna1 = base list
type dna2 = 
    | Nil
    | Cons  of (base * dna2)
let ()=
    let a1:dna1=G::C::A::T::[] in
    let a2:dna2= Cons (G, Cons (C, Cons (A, Cons (T,Nil)))) in
    let firstbase(d:dna2):base =
        match d with
            | Cons (x,y) -> x 
            | Nil  -> failwith "Error" in
    ()

