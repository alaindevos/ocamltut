	.file ""
	.data
	.globl	camlEcho__data_begin
camlEcho__data_begin:
	.text
	.globl	camlEcho__code_begin
camlEcho__code_begin:
	.data
	.align	8
	.data
	.align	8
	.quad	3063
camlEcho__1:
	.quad	camlEcho__echo_80
	.quad	3
	.data
	.align	8
	.quad	1792
	.globl	camlEcho
camlEcho:
	.quad	1
	.data
	.align	8
	.globl	camlEcho__gc_roots
camlEcho__gc_roots:
	.quad	camlEcho
	.quad	0
	.text
	.align	16
	.globl	camlEcho__echo_80
camlEcho__echo_80:
	.cfi_startproc
	subq	$24, %rsp
	.cfi_adjust_cfa_offset 24
.L110:
	movl	$1, %edi
	leaq	caml_sys_argv(%rip), %rax
	call	caml_c_call@PLT
.L100:
	movq	(%r14), %r15
	addq	$-8, %rax
	movq	(%rax), %rax
	shrq	$9, %rax
	orq	$1, %rax
	cmpq	$3, %rax
	jle	.L107
	movq	%rax, (%rsp)
	movl	$1, %edi
	leaq	caml_sys_argv(%rip), %rax
	call	caml_c_call@PLT
.L101:
	movq	(%r14), %r15
	movq	-8(%rax), %rbx
	cmpq	$2047, %rbx
	jbe	.L111
	movq	8(%rax), %rbx
	movq	camlStdlib+304(%rip), %rax
	call	camlStdlib__output_string_245@PLT
.L102:
	movl	$5, %eax
	movq	(%rsp), %rbx
	addq	$-2, %rbx
	cmpq	%rbx, %rax
	jg	.L108
	movq	%rbx, 8(%rsp)
	movq	%rax, (%rsp)
.L109:
	movl	$65, %eax
	call	camlStdlib__print_char_346@PLT
.L103:
	movl	$1, %edi
	leaq	caml_sys_argv(%rip), %rax
	call	caml_c_call@PLT
.L104:
	movq	(%r14), %r15
	movq	-8(%rax), %rbx
	shrq	$9, %rbx
	movq	(%rsp), %rdi
	cmpq	%rdi, %rbx
	jbe	.L111
	movq	-4(%rax,%rdi,4), %rbx
	movq	camlStdlib+304(%rip), %rax
	call	camlStdlib__output_string_245@PLT
.L105:
	movq	(%rsp), %rbx
	movq	%rbx, %rax
	addq	$2, %rbx
	movq	%rbx, (%rsp)
	movq	8(%rsp), %rbx
	cmpq	%rbx, %rax
	jne	.L109
.L108:
	movl	$1, %eax
	addq	$24, %rsp
	.cfi_adjust_cfa_offset -24
	jmp	camlStdlib__print_newline_364@PLT
	.cfi_adjust_cfa_offset 24
	.align	4
.L107:
	movl	$1, %eax
	addq	$24, %rsp
	.cfi_adjust_cfa_offset -24
	ret
	.cfi_adjust_cfa_offset 24
.L111:
	call	caml_ml_array_bound_error@PLT
	.cfi_adjust_cfa_offset -24
	.cfi_endproc
	.text
	.align	16
	.globl	camlEcho__entry
camlEcho__entry:
	.cfi_startproc
	subq	$24, %rsp
	.cfi_adjust_cfa_offset 24
.L122:
	leaq	camlEcho__1(%rip), %rax
	movq	%rax, camlEcho(%rip)
	movl	$1, %edi
	leaq	caml_sys_argv(%rip), %rax
	call	caml_c_call@PLT
.L112:
	movq	(%r14), %r15
	addq	$-8, %rax
	movq	(%rax), %rax
	shrq	$9, %rax
	orq	$1, %rax
	cmpq	$3, %rax
	jle	.L119
	movq	%rax, (%rsp)
	movl	$1, %edi
	leaq	caml_sys_argv(%rip), %rax
	call	caml_c_call@PLT
.L113:
	movq	(%r14), %r15
	movq	-8(%rax), %rbx
	cmpq	$2047, %rbx
	jbe	.L123
	movq	8(%rax), %rbx
	movq	camlStdlib+304(%rip), %rax
	call	camlStdlib__output_string_245@PLT
.L114:
	movl	$5, %eax
	movq	(%rsp), %rbx
	addq	$-2, %rbx
	cmpq	%rbx, %rax
	jg	.L120
	movq	%rbx, 8(%rsp)
	movq	%rax, (%rsp)
.L121:
	movl	$65, %eax
	call	camlStdlib__print_char_346@PLT
.L115:
	movl	$1, %edi
	leaq	caml_sys_argv(%rip), %rax
	call	caml_c_call@PLT
.L116:
	movq	(%r14), %r15
	movq	-8(%rax), %rbx
	shrq	$9, %rbx
	movq	(%rsp), %rdi
	cmpq	%rdi, %rbx
	jbe	.L123
	movq	-4(%rax,%rdi,4), %rbx
	movq	camlStdlib+304(%rip), %rax
	call	camlStdlib__output_string_245@PLT
.L117:
	movq	(%rsp), %rbx
	movq	%rbx, %rax
	addq	$2, %rbx
	movq	%rbx, (%rsp)
	movq	8(%rsp), %rbx
	cmpq	%rbx, %rax
	jne	.L121
.L120:
	movl	$1, %eax
	call	camlStdlib__print_newline_364@PLT
.L118:
.L119:
	movl	$1, %eax
	addq	$24, %rsp
	.cfi_adjust_cfa_offset -24
	ret
	.cfi_adjust_cfa_offset 24
.L123:
	call	caml_ml_array_bound_error@PLT
	.cfi_adjust_cfa_offset -24
	.cfi_endproc
	.data
	.align	8
	.text
	.globl	camlEcho__code_end
camlEcho__code_end:
	.data
				/* relocation table start */
	.align	8
				/* relocation table end */
	.data
	.quad	0
	.globl	camlEcho__data_end
camlEcho__data_end:
	.quad	0
	.align	8
	.globl	camlEcho__frametable
camlEcho__frametable:
	.quad	13
	.quad	.L118
	.word	33
	.word	0
	.align	4
	.long	(.L124 - .) + 0
	.align	8
	.quad	.L117
	.word	33
	.word	0
	.align	4
	.long	(.L125 - .) + 0
	.align	8
	.quad	.L116
	.word	33
	.word	0
	.align	4
	.long	(.L126 - .) + 0
	.align	8
	.quad	.L115
	.word	33
	.word	0
	.align	4
	.long	(.L127 - .) + 0
	.align	8
	.quad	.L114
	.word	33
	.word	0
	.align	4
	.long	(.L125 - .) + 0
	.align	8
	.quad	.L113
	.word	33
	.word	0
	.align	4
	.long	(.L128 - .) + 0
	.align	8
	.quad	.L112
	.word	33
	.word	0
	.align	4
	.long	(.L129 - .) + 0
	.align	8
	.quad	.L105
	.word	33
	.word	0
	.align	4
	.long	(.L125 - .) + 0
	.align	8
	.quad	.L104
	.word	33
	.word	0
	.align	4
	.long	(.L126 - .) + 0
	.align	8
	.quad	.L103
	.word	33
	.word	0
	.align	4
	.long	(.L127 - .) + 0
	.align	8
	.quad	.L102
	.word	33
	.word	0
	.align	4
	.long	(.L125 - .) + 0
	.align	8
	.quad	.L101
	.word	33
	.word	0
	.align	4
	.long	(.L128 - .) + 0
	.align	8
	.quad	.L100
	.word	33
	.word	0
	.align	4
	.long	(.L129 - .) + 0
	.align	8
	.align	4
.L125:
	.long	(.L131 - .) + -1409286144
	.long	1966416
	.align	4
.L128:
	.long	(.L133 - .) + 1677721600
	.long	24848
	.align	4
.L126:
	.long	(.L133 - .) + 1744830464
	.long	37152
	.align	4
.L129:
	.long	(.L133 - .) + -2147483648
	.long	12672
	.align	4
.L127:
	.long	(.L133 - .) + 1275068416
	.long	32848
	.align	4
.L124:
	.long	(.L133 - .) + 1342177280
	.long	45120
.L130:
	.ascii	"stdlib.ml\0"
.L132:
	.ascii	"echo.ml\0"
	.align	4
.L131:
	.long	(.L130 - .) + 0
	.ascii	"Stdlib.print_string\0"
	.align	4
.L133:
	.long	(.L132 - .) + 0
	.ascii	"Echo.echo\0"
	.align	8
