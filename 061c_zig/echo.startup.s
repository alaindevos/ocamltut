	.file ""
	.data
	.globl	caml_startup__data_begin
caml_startup__data_begin:
	.text
	.globl	caml_startup__code_begin
caml_startup__code_begin:
	.text
	.align	16
	.globl	caml_program
caml_program:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
.L139:
	call	camlCamlinternalFormatBasics__entry@PLT
.L134:
	addq	$1, caml_globals_inited(%rip)
	call	camlStdlib__entry@PLT
.L135:
	addq	$1, caml_globals_inited(%rip)
	call	camlStdlib__sys__entry@PLT
.L136:
	addq	$1, caml_globals_inited(%rip)
	call	camlEcho__entry@PLT
.L137:
	addq	$1, caml_globals_inited(%rip)
	call	camlStd_exit__entry@PLT
.L138:
	addq	$1, caml_globals_inited(%rip)
	movl	$1, %eax
	addq	$8, %rsp
	.cfi_adjust_cfa_offset -8
	ret
	.cfi_adjust_cfa_offset 8
	.cfi_adjust_cfa_offset -8
	.cfi_endproc
	.text
	.align	16
	.globl	caml_curry4
caml_curry4:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
.L140:
	subq	$48, %r15
	cmpq	8(%r14), %r15
	jb	.L141
.L143:
	leaq	8(%r15), %rdi
	movq	$5367, -8(%rdi)
	leaq	caml_curry4_1(%rip), %rsi
	movq	%rsi, (%rdi)
	movq	$7, 8(%rdi)
	leaq	caml_curry4_1_app(%rip), %rsi
	movq	%rsi, 16(%rdi)
	movq	%rax, 24(%rdi)
	movq	%rbx, 32(%rdi)
	movq	%rdi, %rax
	addq	$8, %rsp
	.cfi_adjust_cfa_offset -8
	ret
	.cfi_adjust_cfa_offset 8
.L141:
	call	caml_call_gc@PLT
.L142:
	jmp	.L143
	.cfi_adjust_cfa_offset -8
	.cfi_endproc
	.text
	.align	16
	.globl	caml_curry4_1_app
caml_curry4_1_app:
	.cfi_startproc
.L145:
	movq	%rax, %rcx
	movq	%rbx, %r8
	movq	%rdi, %r9
	movq	32(%rsi), %rdx
	movq	24(%rsi), %rax
	movq	16(%rdx), %r12
	movq	%rcx, %rbx
	movq	%r8, %rdi
	movq	%r9, %rsi
	jmp	*%r12
	.cfi_endproc
	.text
	.align	16
	.globl	caml_curry4_1
caml_curry4_1:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
.L146:
	subq	$48, %r15
	cmpq	8(%r14), %r15
	jb	.L147
.L149:
	leaq	8(%r15), %rdi
	movq	$5367, -8(%rdi)
	leaq	caml_curry4_2(%rip), %rsi
	movq	%rsi, (%rdi)
	movq	$5, 8(%rdi)
	leaq	caml_curry4_2_app(%rip), %rsi
	movq	%rsi, 16(%rdi)
	movq	%rax, 24(%rdi)
	movq	%rbx, 32(%rdi)
	movq	%rdi, %rax
	addq	$8, %rsp
	.cfi_adjust_cfa_offset -8
	ret
	.cfi_adjust_cfa_offset 8
.L147:
	call	caml_call_gc@PLT
.L148:
	jmp	.L149
	.cfi_adjust_cfa_offset -8
	.cfi_endproc
	.text
	.align	16
	.globl	caml_curry4_2_app
caml_curry4_2_app:
	.cfi_startproc
.L151:
	movq	%rax, %rcx
	movq	%rbx, %rsi
	movq	32(%rdi), %rax
	movq	32(%rax), %rdx
	movq	24(%rdi), %rbx
	movq	24(%rax), %rax
	movq	16(%rdx), %r8
	movq	%rcx, %rdi
	jmp	*%r8
	.cfi_endproc
	.text
	.align	16
	.globl	caml_curry4_2
caml_curry4_2:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
.L152:
	subq	$40, %r15
	cmpq	8(%r14), %r15
	jb	.L153
.L155:
	leaq	8(%r15), %rdi
	movq	$4343, -8(%rdi)
	leaq	caml_curry4_3(%rip), %rsi
	movq	%rsi, (%rdi)
	movq	$3, 8(%rdi)
	movq	%rax, 16(%rdi)
	movq	%rbx, 24(%rdi)
	movq	%rdi, %rax
	addq	$8, %rsp
	.cfi_adjust_cfa_offset -8
	ret
	.cfi_adjust_cfa_offset 8
.L153:
	call	caml_call_gc@PLT
.L154:
	jmp	.L155
	.cfi_adjust_cfa_offset -8
	.cfi_endproc
	.text
	.align	16
	.globl	caml_curry4_3
caml_curry4_3:
	.cfi_startproc
.L157:
	movq	%rax, %rsi
	movq	24(%rbx), %rax
	movq	32(%rax), %rcx
	movq	32(%rcx), %rdx
	movq	16(%rbx), %rdi
	movq	24(%rax), %rbx
	movq	24(%rcx), %rax
	movq	16(%rdx), %rcx
	jmp	*%rcx
	.cfi_endproc
	.text
	.align	16
	.globl	caml_curry3
caml_curry3:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
.L158:
	subq	$48, %r15
	cmpq	8(%r14), %r15
	jb	.L159
.L161:
	leaq	8(%r15), %rdi
	movq	$5367, -8(%rdi)
	leaq	caml_curry3_1(%rip), %rsi
	movq	%rsi, (%rdi)
	movq	$5, 8(%rdi)
	leaq	caml_curry3_1_app(%rip), %rsi
	movq	%rsi, 16(%rdi)
	movq	%rax, 24(%rdi)
	movq	%rbx, 32(%rdi)
	movq	%rdi, %rax
	addq	$8, %rsp
	.cfi_adjust_cfa_offset -8
	ret
	.cfi_adjust_cfa_offset 8
.L159:
	call	caml_call_gc@PLT
.L160:
	jmp	.L161
	.cfi_adjust_cfa_offset -8
	.cfi_endproc
	.text
	.align	16
	.globl	caml_curry3_1_app
caml_curry3_1_app:
	.cfi_startproc
.L163:
	movq	%rax, %rdx
	movq	%rbx, %rcx
	movq	32(%rdi), %rsi
	movq	24(%rdi), %rax
	movq	16(%rsi), %r8
	movq	%rdx, %rbx
	movq	%rcx, %rdi
	jmp	*%r8
	.cfi_endproc
	.text
	.align	16
	.globl	caml_curry3_1
caml_curry3_1:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
.L164:
	subq	$40, %r15
	cmpq	8(%r14), %r15
	jb	.L165
.L167:
	leaq	8(%r15), %rdi
	movq	$4343, -8(%rdi)
	leaq	caml_curry3_2(%rip), %rsi
	movq	%rsi, (%rdi)
	movq	$3, 8(%rdi)
	movq	%rax, 16(%rdi)
	movq	%rbx, 24(%rdi)
	movq	%rdi, %rax
	addq	$8, %rsp
	.cfi_adjust_cfa_offset -8
	ret
	.cfi_adjust_cfa_offset 8
.L165:
	call	caml_call_gc@PLT
.L166:
	jmp	.L167
	.cfi_adjust_cfa_offset -8
	.cfi_endproc
	.text
	.align	16
	.globl	caml_curry3_2
caml_curry3_2:
	.cfi_startproc
.L169:
	movq	%rax, %rdi
	movq	24(%rbx), %rax
	movq	32(%rax), %rsi
	movq	16(%rbx), %rbx
	movq	24(%rax), %rax
	movq	16(%rsi), %rdx
	jmp	*%rdx
	.cfi_endproc
	.text
	.align	16
	.globl	caml_curry2
caml_curry2:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
.L170:
	subq	$40, %r15
	cmpq	8(%r14), %r15
	jb	.L171
.L173:
	leaq	8(%r15), %rdi
	movq	$4343, -8(%rdi)
	leaq	caml_curry2_1(%rip), %rsi
	movq	%rsi, (%rdi)
	movq	$3, 8(%rdi)
	movq	%rax, 16(%rdi)
	movq	%rbx, 24(%rdi)
	movq	%rdi, %rax
	addq	$8, %rsp
	.cfi_adjust_cfa_offset -8
	ret
	.cfi_adjust_cfa_offset 8
.L171:
	call	caml_call_gc@PLT
.L172:
	jmp	.L173
	.cfi_adjust_cfa_offset -8
	.cfi_endproc
	.text
	.align	16
	.globl	caml_curry2_1
caml_curry2_1:
	.cfi_startproc
.L175:
	movq	%rax, %rsi
	movq	24(%rbx), %rdi
	movq	16(%rbx), %rax
	movq	16(%rdi), %rdx
	movq	%rsi, %rbx
	jmp	*%rdx
	.cfi_endproc
	.text
	.align	16
	.globl	caml_apply3
caml_apply3:
	.cfi_startproc
	subq	$24, %rsp
	.cfi_adjust_cfa_offset 24
.L181:
	movq	8(%rsi), %rdx
	cmpq	$7, %rdx
	jne	.L180
	movq	16(%rsi), %rdx
	addq	$24, %rsp
	.cfi_adjust_cfa_offset -24
	jmp	*%rdx
	.cfi_adjust_cfa_offset 24
	.align	4
.L180:
	movq	%rdi, 8(%rsp)
	movq	%rbx, (%rsp)
	movq	(%rsi), %rdi
	movq	%rsi, %rbx
	call	*%rdi
.L176:
	movq	%rax, %rbx
	movq	(%rbx), %rdi
	movq	(%rsp), %rax
	call	*%rdi
.L177:
	movq	%rax, %rbx
	movq	(%rbx), %rdi
	movq	8(%rsp), %rax
	addq	$24, %rsp
	.cfi_adjust_cfa_offset -24
	jmp	*%rdi
	.cfi_adjust_cfa_offset 24
	.cfi_adjust_cfa_offset -24
	.cfi_endproc
	.text
	.align	16
	.globl	caml_apply2
caml_apply2:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
.L186:
	movq	8(%rdi), %rsi
	cmpq	$5, %rsi
	jne	.L185
	movq	16(%rdi), %rsi
	addq	$8, %rsp
	.cfi_adjust_cfa_offset -8
	jmp	*%rsi
	.cfi_adjust_cfa_offset 8
	.align	4
.L185:
	movq	%rbx, (%rsp)
	movq	(%rdi), %rsi
	movq	%rdi, %rbx
	call	*%rsi
.L182:
	movq	%rax, %rbx
	movq	(%rbx), %rdi
	movq	(%rsp), %rax
	addq	$8, %rsp
	.cfi_adjust_cfa_offset -8
	jmp	*%rdi
	.cfi_adjust_cfa_offset 8
	.cfi_adjust_cfa_offset -8
	.cfi_endproc
	.data
	.align	8
	.quad	3064
	.globl	caml_exn_Out_of_memory
caml_exn_Out_of_memory:
	.quad	caml_startup__2
	.quad	-1
	.quad	3068
caml_startup__2:
	.ascii	"Out_of_memory"
	.space	2
	.byte	2
	.data
	.align	8
	.quad	3064
	.globl	caml_exn_Sys_error
caml_exn_Sys_error:
	.quad	caml_startup__3
	.quad	-3
	.quad	3068
caml_startup__3:
	.ascii	"Sys_error"
	.space	6
	.byte	6
	.data
	.align	8
	.quad	3064
	.globl	caml_exn_Failure
caml_exn_Failure:
	.quad	caml_startup__4
	.quad	-5
	.quad	2044
caml_startup__4:
	.ascii	"Failure"
	.byte	0
	.data
	.align	8
	.quad	3064
	.globl	caml_exn_Invalid_argument
caml_exn_Invalid_argument:
	.quad	caml_startup__5
	.quad	-7
	.quad	4092
caml_startup__5:
	.ascii	"Invalid_argument"
	.space	7
	.byte	7
	.data
	.align	8
	.quad	3064
	.globl	caml_exn_End_of_file
caml_exn_End_of_file:
	.quad	caml_startup__6
	.quad	-9
	.quad	3068
caml_startup__6:
	.ascii	"End_of_file"
	.space	4
	.byte	4
	.data
	.align	8
	.quad	3064
	.globl	caml_exn_Division_by_zero
caml_exn_Division_by_zero:
	.quad	caml_startup__7
	.quad	-11
	.quad	4092
caml_startup__7:
	.ascii	"Division_by_zero"
	.space	7
	.byte	7
	.data
	.align	8
	.quad	3064
	.globl	caml_exn_Not_found
caml_exn_Not_found:
	.quad	caml_startup__8
	.quad	-13
	.quad	3068
caml_startup__8:
	.ascii	"Not_found"
	.space	6
	.byte	6
	.data
	.align	8
	.quad	3064
	.globl	caml_exn_Match_failure
caml_exn_Match_failure:
	.quad	caml_startup__9
	.quad	-15
	.quad	3068
caml_startup__9:
	.ascii	"Match_failure"
	.space	2
	.byte	2
	.data
	.align	8
	.quad	3064
	.globl	caml_exn_Stack_overflow
caml_exn_Stack_overflow:
	.quad	caml_startup__10
	.quad	-17
	.quad	3068
caml_startup__10:
	.ascii	"Stack_overflow"
	.space	1
	.byte	1
	.data
	.align	8
	.quad	3064
	.globl	caml_exn_Sys_blocked_io
caml_exn_Sys_blocked_io:
	.quad	caml_startup__11
	.quad	-19
	.quad	3068
caml_startup__11:
	.ascii	"Sys_blocked_io"
	.space	1
	.byte	1
	.data
	.align	8
	.quad	3064
	.globl	caml_exn_Assert_failure
caml_exn_Assert_failure:
	.quad	caml_startup__12
	.quad	-21
	.quad	3068
caml_startup__12:
	.ascii	"Assert_failure"
	.space	1
	.byte	1
	.data
	.align	8
	.quad	3064
	.globl	caml_exn_Undefined_recursive_module
caml_exn_Undefined_recursive_module:
	.quad	caml_startup__13
	.quad	-23
	.quad	5116
caml_startup__13:
	.ascii	"Undefined_recursive_module"
	.space	5
	.byte	5
	.data
	.align	8
	.globl	caml_globals
caml_globals:
	.quad	camlCamlinternalFormatBasics__gc_roots
	.quad	camlStdlib__gc_roots
	.quad	camlStdlib__sys__gc_roots
	.quad	camlEcho__gc_roots
	.quad	camlStd_exit__gc_roots
	.quad	0
	.data
	.align	8
	.quad	179196
	.globl	caml_globals_map
caml_globals_map:
	.ascii	"\204\225\246\276\0\0\5U\0\0\0\313\0\0\3d\0\0\2\343\240\300.Stdlib__lexing@@@\240\300-Stdlib__int64@@@\240\300+Stdlib__int@@@\240\300.Stdlib__string@@@\240\300.Stdlib__format@@@\240\300,Stdlib__bool@@@\240\300.Stdlib__genlex@@@\240\300/Stdlib__complex@@@\240\300.Stdlib__result@@@\240\300+Stdlib__obj@@@\240\300\62Stdlib__listLabels@@@\240\300+Stdlib__fun@@@\240\300,Stdlib__char@@@\240\300\61Stdlib__spacetime@@@\240\300-Stdlib__float@@@\240\300.Stdlib__buffer@@@\240\300-Stdlib__bytes@@@\240\300\62Stdlib__pervasives@@@\240\300\60Stdlib__filename@@@\240\300.Stdlib__random@@@\240\300\60Stdlib__callback@@@\240\300\64Stdlib__stringLabels@@@\240\300.Stdlib__option@@@\240\300.Stdlib__stream@@@\240\300+Stdlib__set@@@\240\300-Stdlib__queue@@@\240\300\61Stdlib__nativeint@@@\240\300\60Stdlib__bigarray@@@\240\300,Stdlib__lazy@@@\240\300\63Stdlib__bytesLabels@@@\240\300\61Stdlib__stdLabels@@@\240\300,Stdlib__list@@@\240\300*Stdlib__gc@@@\240\300-Stdlib__uchar@@@\240\300/Stdlib__parsing@@@\240\300/Stdlib__hashtbl@@@\240\300*Stdlib__oo@@@\240\300\62Stdlib__moreLabels@@@\240\300,Stdlib__unit@@@\240\300.Stdlib__printf@@@\240\300.Stdlib__digest@@@\240\300-Stdlib__scanf@@@\240\300-Stdlib__int32@@@\240\300+Stdlib__arg@@@\240\300+Stdlib__map@@@\240\300-Stdlib__stack@@@\240\300\60Stdlib__printexc@@@\240\300/Stdlib__marshal@@@\240\300\61Stdlib__ephemeron@@@\240\300\63Stdlib__arrayLabels@@@\240\300,Stdlib__weak@@@\240\300+Stdlib__seq\220\60\326\250\336%\311\356\317Z\351B\12\237?\213.\210@@\240\300-Stdlib__array\220\60E(\313\177\42\2\251m\367\274)5\231\316A\254@@\240\300\70CamlinternalFormatBasics\220\60:<\241\203\206'\367v/Ig\234\340'\212\321\220\60\260K\257\266\317\211\321\245Ns[\257\343\225\246\30\240\4\6@\240\300&Stdlib\220\60\302\34]&Ada\265C2\30r\245Q\352\15\220\60[\21\30\374\223\204#\3y3\346\66C\351\366q\240\4\6@\240\300+Stdlib__sys\220\60yI$\267\206\325\303\5\234T\134\265&9E \220\60B\326~S\241 \260\260\66\310W\366Qt\222\223\240\4\6@\240\300$Echo\220\60\370\253\314\11\365y\31\34F\314\21\303\331\301\226\243\220\60\266:\271\344\262XG\244:\25\7o2\327\260\233\240\4\6@\240\300(Std_exit\220\60*\350dS\354\4\212\230Y1\310\1\320j\250\202\220\60\227#\220\343TM\372\321\33%{dU\250\11\255\240\4\6@@"
	.space	6
	.byte	6
	.data
	.align	8
	.globl	caml_data_segments
caml_data_segments:
	.quad	caml_startup__data_begin
	.quad	caml_startup__data_end
	.quad	camlCamlinternalFormatBasics__data_begin
	.quad	camlCamlinternalFormatBasics__data_end
	.quad	camlStdlib__data_begin
	.quad	camlStdlib__data_end
	.quad	camlStdlib__sys__data_begin
	.quad	camlStdlib__sys__data_end
	.quad	camlEcho__data_begin
	.quad	camlEcho__data_end
	.quad	camlStd_exit__data_begin
	.quad	camlStd_exit__data_end
	.quad	0
	.data
	.align	8
	.globl	caml_code_segments
caml_code_segments:
	.quad	caml_startup__code_begin
	.quad	caml_startup__code_end
	.quad	camlCamlinternalFormatBasics__code_begin
	.quad	camlCamlinternalFormatBasics__code_end
	.quad	camlStdlib__code_begin
	.quad	camlStdlib__code_end
	.quad	camlStdlib__sys__code_begin
	.quad	camlStdlib__sys__code_end
	.quad	camlEcho__code_begin
	.quad	camlEcho__code_end
	.quad	camlStd_exit__code_begin
	.quad	camlStd_exit__code_end
	.quad	0
	.data
	.align	8
	.globl	caml_frametable
caml_frametable:
	.quad	caml_startup__frametable
	.quad	caml_system__frametable
	.quad	camlCamlinternalFormatBasics__frametable
	.quad	camlStdlib__frametable
	.quad	camlStdlib__sys__frametable
	.quad	camlEcho__frametable
	.quad	camlStd_exit__frametable
	.quad	0
	.text
	.globl	caml_startup__code_end
caml_startup__code_end:
	.data
				/* relocation table start */
	.align	8
				/* relocation table end */
	.data
	.quad	0
	.globl	caml_startup__data_end
caml_startup__data_end:
	.quad	0
	.align	8
	.globl	caml_startup__frametable
caml_startup__frametable:
	.quad	14
	.quad	.L182
	.word	16
	.word	1
	.word	0
	.align	8
	.quad	.L177
	.word	32
	.word	1
	.word	8
	.align	8
	.quad	.L176
	.word	32
	.word	2
	.word	0
	.word	8
	.align	8
	.quad	.L172
	.word	18
	.word	2
	.word	1
	.word	3
	.byte	1
	.byte	3
	.align	8
	.quad	.L166
	.word	18
	.word	2
	.word	1
	.word	3
	.byte	1
	.byte	3
	.align	8
	.quad	.L160
	.word	18
	.word	2
	.word	1
	.word	3
	.byte	1
	.byte	4
	.align	8
	.quad	.L154
	.word	18
	.word	2
	.word	1
	.word	3
	.byte	1
	.byte	3
	.align	8
	.quad	.L148
	.word	18
	.word	2
	.word	1
	.word	3
	.byte	1
	.byte	4
	.align	8
	.quad	.L142
	.word	18
	.word	2
	.word	1
	.word	3
	.byte	1
	.byte	4
	.align	8
	.quad	.L138
	.word	16
	.word	0
	.align	8
	.quad	.L137
	.word	16
	.word	0
	.align	8
	.quad	.L136
	.word	16
	.word	0
	.align	8
	.quad	.L135
	.word	16
	.word	0
	.align	8
	.quad	.L134
	.word	16
	.word	0
	.align	8
	.align	8
