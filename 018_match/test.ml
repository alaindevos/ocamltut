(*Match ***************************************************************)
let myeven m =
   match m with
   | 0 -> true
   | 2 -> true
   | 4 -> true
   | _ -> false ;;
Printf.printf "\n %b" (myeven 1) ;;

let swap p =
   match p with
   | (x,y) -> (y,x) ;;

let printpair ppf (x,y) =
   Printf.printf "\n %d %d" x y ;;

Printf.printf "%a" printpair (swap(3,7)) ;;

let rec mysum xs =
   match xs with 
   | [] -> 0
   | f::r -> f + (mysum r) ;; 
Printf.printf "\n %d" (mysum[1;2;3;]) ;;

let x=
    match not true with
        | true -> "yes"
        | false -> "no";;
let y=
    match 42 with
        | myvar -> myvar;;
let z=
    match "fooo" with
        | "bar" -> 0
        | _ -> 1;;
let a=
    match [] with
        | [] -> "empty"
        | _ ->  "not empty";;
let b1=
    match ["taylor";"swift";"song"] with
       | [] -> "nothing"
       | h::t -> 
               print_string (h) ;
               print_string (List.hd t) ;
               "something";;
(* First component of a triple *)
let fst3 t = 
    match t with
    | (a,b,c) -> a;;
type student= {name:string;year:int;}
let getnameyear (s:student):string=
    match s with
    | {name;year}-> name^" "^(string_of_int year);;

let isemptylist lst=
    match lst with
    | [] -> true
    | _::_ -> false;;

let rec sumlist lst =
    match lst with
    | [] -> 0
    | h::t -> h + (sumlist t);;

let rec lengthlist lst =
    match lst with
    | [] -> 0
    | h::t -> 1 + (lengthlist t);;

let rec appendlist lst1 lst2 =
    match lst1 with
    | [] -> lst2
    | h::t -> h::(appendlist t lst2);;

let rec mylen lst = match lst with
    | [] -> 0
    | h::t -> 1 + (mylen t);;


