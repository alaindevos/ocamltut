(* sequence of integers, natural numbers ******************)
type intseq= Cons of int * ( unit -> intseq)
let _=
    let rec from n = Cons (n,fun()->from(n+1)) in
    let nats=from 0 in
    let head (Cons (h,_))=h in 
    let tail (Cons (_,t))=t in
    let rec mytake (num:int) (seq:intseq)=
        if num=0 then []
        else head seq :: mytake (num-1) (tail seq () ) in
    let take10=mytake 10 nats in
    print_int (List.hd (List.tl (List.tl take10)))
