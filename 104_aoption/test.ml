type 'a aoption= None | Some of 'a
let i0: (int aoption)=None;;
let i1: (int aoption)=Some 1;;
let i2: (int aoption)=Some 2;;
let f0: (float aoption)=None;;
let f1: (float aoption)=Some 1.0;;

let getval1 (x:('a aoption)):'a = match x with
    | None -> failwith "Error"
    | Some x -> x 


let getval2 (adefault:'a) (x:('a option)):'a = match x with
    | None -> adefault 
    | Some x -> x 

let mydivi (x:'a) (y:'a):('a option)= match y with
    | 0 -> None 
    | _ -> Some (x / y );; 

let mydivf (x:'a) (y:'a):('a option)= match y with
    | 0.0 -> None 
    | _ -> Some (x /. y );; 

let intmax (x:'a) (y:'a):'a = max x y;;

let rec listmax (lst: int list):('a aoption)=match lst with
    | [] -> None
    | h::t -> 
            begin
                match listmax t with
                | None -> Some h
                | Some x -> Some (intmax h x)
            end;;

