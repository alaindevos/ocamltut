(* Get two last elements of a list ********************************)
type twostring=string*string 
type sometwostring= None | Some of twostring
type stringlist=string list 

let fgetstring (x:sometwostring):string*string = match x with
    | None -> ("None","None")
    | Some (x,y) -> (x,y);;

let fprinttwolast (x:sometwostring)=
    let y=fgetstring x in 
    let (a,b)=y in 
    let ()=print_string a in
    let ()=print_string b in 
    ();;

let rec fgettwolast (lst:stringlist):sometwostring =  match lst with 
    | [] -> None 
    | [x] -> None
    | [x;y] -> Some (x,y) 
    | _::t -> fgettwolast t;;

let ()=
    let ()=fprinttwolast ( fgettwolast ["aa";"bb";"cc";"dd"]) in 
    let ()=fprinttwolast ( fgettwolast []) in 
    ();;
