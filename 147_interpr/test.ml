
type varspec = string

type binop = 
    | Minus
    | Divide  

type expr =
    | Int of int
    | Var of varspec
    | Binop of (binop*expr*expr)
    | Let of (varspec*expr*expr)

let ()=
    let e:expr=
        Let (  "x",
             Int 1,
              Let (  "y",
                   Int 2,
                  Binop ( Minus,
                        Var "x",
                        Var "y"))) in

    let rec subst (inp:expr) (varx:varspec) (valq:expr) =  match inp with
        | Int i -> Int i
        | Var  v -> if (v = varx) 
                        then valq
                        else Var v
        | Binop  (b,e1,e2) -> Binop  (b, subst e1 varx valq, subst e2 varx valq)
        | Let (v,e1,e2) -> if (v=varx) 
                                then
                                    e1
                                else
                                    e2 in
    ()

