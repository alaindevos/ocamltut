(* Four times same function *)
let f1(x:unit):unit=print_int 1
let f2(_:unit):unit=print_int 2
let f3():unit=print_int 3
let f4()=print_int  4

let main()=
    f1 ();
    f2 ();
    f3 ();
    f4 ()

let _= main ()
