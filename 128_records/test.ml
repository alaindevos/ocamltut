type db_entry =
    {
        name : string;
        height : float;
        phone: string;
        salary : float;
    }

type anamex =
    { mutable namex : string}

let _ =
    let jason = 
        {
            name="Jason";
            height=6.25;
            phone="053662962";
            salary=1500.0;
    } in
    print_string jason.phone;
    (*pattern matching *)
    let {name=x}  =jason in
    print_string x;

    let george =
        {namex="George"}  in
    george.namex <- "Koekoek";
()



