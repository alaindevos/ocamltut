open Unix
open Sys

let counter = ref 0

let handler (x : int) : unit =
  print_endline "";
  counter := 0;
  print_string "RESET";
  print_endline ""

let handle : Sys.signal_behavior = Signal_handle handler;;

signal sigint handle;;

let (_ : unit) =
  while true do
    sleep 1;
    counter := !counter + 1;
    print_int !counter;
    print_endline ""
  done
in
()
