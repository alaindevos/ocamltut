(* An array has fixed length , all elements have same type *****************)
let x =[| 1;2;3|];;
x.(1)=9;;
let y=Array.make 10 0 ;; (* 10 elements with init value 0 *)
let a=[|1;2;3;4;5|];;
let f=a.(0);;
print_int f;;
a.(0) <- 55;;
print_int a.(0);;
print_int (Array.length a);;
let b=Array.make 5 "AlainDevos";;
print_string b.(0);;
let c=char_of_int 65;;
print_char c;;
let printchar c=print_char c;;
String.iter printchar "Eddy";;
