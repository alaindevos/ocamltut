(* Binary tree of ints **************************************************)
type inttree =
    | Leaf
    | Node of int*inttree*inttree
let t=Node (1,
            Node(2,Leaf,Leaf),
            Node(3,Leaf,Leaf));;
let rec treesize = function
    | Leaf -> 0
    | Node (_,left,right) -> 1 + (treesize left) + (treesize right);;
let rec treesum = function
    | Leaf -> 0
    | Node (n,left,right) -> n + (treesum left) + (treesum right)
