(* Clojure / local state *)

type state={mutable count:int}

let global:state={count=0}

let incrG(_:unit):int =
    global.count<-global.count+1;
    global.count

let mk_incr(_:unit):(unit->int)=
    let local:state={count=0} in 
    let incr (_:unit):int=
        local.count<-local.count+1;
        local.count
    in
    incr 

let main()=
    print_int (incrG ());
    print_int (incrG ());
    let x():(unit->int) = mk_incr () in  
    let xinc:(unit->int) = x () in
    print_int (xinc ());
    print_int (xinc ());
    ()

let _= main ()

