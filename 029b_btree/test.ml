(* Tree of ints ****************************************************)
type mtree =
    | Br of int * mtree * mtree
    | Lf;;
let a= Br(1,Lf,Lf);;
let b= Br(2,a,a);;
let rec mytotal t=match t with
    | Br(x,y,z) -> x+(mytotal y)+(mytotal z)
    | Lf -> 0;;
Printf.printf "%d\n" (mytotal b);;
let rec mtree2list t=match t with
    | Br(x,l,r)-> (mtree2list l) @ [x] @ (mtree2list r)
    | Lf -> [];;
