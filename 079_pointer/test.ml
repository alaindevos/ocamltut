(* pointer *******************************************)
type intpointer= int ref option
let () =
    let malloc (x:int): intpointer = Some (ref x) in 
    let deref (ptr:intpointer):int =match ptr with 
        | None -> 0   
        | Some x -> !x in 
    let null:intpointer= None in 
    let p:intpointer= malloc 42 in 
    let i:int=deref p in 
    let ()= print_int i in
    let p:intpointer=null in 
    let ()= print_int (deref p) in 
    () 


