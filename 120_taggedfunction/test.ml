type sp= 
    | Sum
    | Product

let _ =
    let f= fun (x:sp) (y:int) (z:int) : int  ->
        match x with
        | Sum  -> y+z
        | Product  -> y * z in
    print_int (f Sum 2 3);
    print_int (f Product 2 3);
    ()
