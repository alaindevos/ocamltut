(* records= tuples with named fields ******************************************)
type vector = {x:float ; mutable y:float};;
let r:vector={x=0.;y=1.};;
Printf.printf "%f" (r.x +. r.y);;
r.y <- 7.;;
print_string (string_of_float r.y);;
