(* pipeline ********************************************************)
let _=
    let double x = 2 * x in
    let square x = x * x in
    let apply f x = f x in 
    let pipeline x f = f x in 
    let ( |> ) = pipeline in 
    let compose f g x = f (g x) in
    let x = apply double 2 in
    print_int x;
    let y=  2 |> square |> double in
    print_int y; 
    let z= compose double double 2 in
    print_int z;
    ()
