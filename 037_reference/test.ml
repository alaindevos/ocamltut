(* reference ***********************************************************)
let x:int ref=ref 0;;
x:=!x+1;;
print_int !x;;
let x=ref 0;;
let x2= !x;;
print_int x2;;
x:=3;;
let x3= !x;;
print_int x2;;
print_int x3;;

let myswap a b=
    let t = !a in
    a:= !b;
    b:=t;;
let x=ref 0;;
let y=ref 1;; 
myswap x y;;
print_int !x;;
print_int !y;;

if 1=1 then
    begin
        x:= !x+1;
        y:= !y-1;
    end
else
    ();;

for x=1 to 5 
    do
        print_newline ();
        print_int x
    done;;

let t=ref 1 in
while !t <=5 
    do 
        print_newline();
        print_int !t;
        t:= !t+1
    done;;

(*working with files *************************************************)
let countlines ch=
    let num=ref 0 in
    try
        while true
            do
                 let _=input_line ch in ();
                 num:= !num+1
            done
    with
        End_of_file ->
            print_newline ();
            print_int !num;;

let filelines name=
    let ch=open_in name in
    try
        countlines ch;
        close_in ch
    with
         _ -> close_in ch;;
filelines "test.txt";;
