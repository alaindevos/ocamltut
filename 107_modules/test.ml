module A : sig
        type nil =NIL 
        val idem:nil->nil 
        val f1:unit->unit
     end = struct
        type nil = NIL 
        let idem = fun x -> x
        let f1 = fun() ->  print_endline "\n A \n"
    end 

module B :sig
        val f2:unit
    end = struct
        let f2 = print_endline "\b B  \n"
    end

let main () =
    let () = print_endline "Start main" in 
    let _:A.nil = A.NIL in
    let _:A.nil = A.idem(A.NIL) in 
    let _:A.nil = A.idem(NIL) in   (* shorthand *) 
    let ()= A.f1 () in 
    let ()= B.f2 in  (* Does nothing as already been evaluated before start main *)  
    let ()= print_endline "Stop main" in 
    () 

let ()= main ()
