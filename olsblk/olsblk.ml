let () =
    let nam2dev = ref [ ("", "") ] in
    let dev2nam = ref [ ("", "") ] in
    let dev2mount = ref [ ("", "") ] in
    let dev2ro = ref [ ("", "") ] in
    let read_process_lines (command:string) : (string list) =
        let lines = ref [] in
        let in_channel = Unix.open_process_in command in
        (try
           while true do
             lines := input_line in_channel :: !lines
           done
         with End_of_file -> ignore (Unix.close_process_in in_channel));
        List.rev !lines
    in
    let rec mylookupkv (sk:string) (alist:(string*string)list) : string =
        match alist with
        | [] -> ""
        | (key, value) :: tail -> if sk = key then value else mylookupkv sk tail
    in
    let regexpos regex line : int =
        try Str.search_forward regex line 0 with Not_found -> -1
    in
    let spacereg = Str.regexp " +" in
    let glabelline line =
        let lst = Str.split spacereg line in
        let n = "/dev/" ^ List.hd lst in
        let d = "/dev/" ^ List.nth lst 1 in
        nam2dev := (n, d) :: !nam2dev;
        dev2nam := (d, n) :: !dev2nam
    in
    let mountline line =
        let lst = Str.split spacereg line in
        let w = List.hd lst in
        let t = List.nth lst 2 in
        let v = mylookupkv w !nam2dev in
        let w2 = if v <> "" then mylookupkv w !nam2dev else w in
        dev2mount := (w2, t) :: !dev2mount;
        let roreg = Str.regexp "read-only" in
        let pos = regexpos roreg line in
        if pos <> -1 then dev2ro := (w2, "ro") :: !dev2ro
        else dev2ro := (w2, "rw") :: !dev2ro
    in
    let id_to_command (cid:string) : string = "ps -p " ^ cid ^ " -o command | sed 1d | sort -u" in
    let ntfsline line =
        let lst = Str.split spacereg line in
        let id = List.hd lst in
        let command = id_to_command id in
        let pstxt = read_process_lines command in
        let lst2 = Str.split spacereg (List.hd pstxt) in
        let d = List.nth lst2 3 in
        let m = List.nth lst2 4 in
        let () =
            let () = dev2ro := (d, "?") :: !dev2ro in
            let () = dev2mount := (d, m) :: !dev2mount in
            ()
        in
        ()
    in
    let eqline (x:bool) line  =
        let len = String.length line in
        if x then String.sub line 2 (len - 2) else line
    in
    let ismin (x:bool) (line:string) :string = if x then "<Free>" else line in
    let mountval (x:string) (d:string) : string = if x = "" then "-" else mylookupkv d !dev2mount in
    let roval (x:string) (d:string) : string = if x = "" then "-" else mylookupkv d !dev2ro in
    let padme (x:int) (s:string) : string =
        let numspaces = x - String.length s in
        let spacesref = ref "" in
        for i = 0 to numspaces do
          spacesref := " " ^ !spacesref
        done;
        let sum = s ^ !spacesref in
        sum
    in
    let gpartline (line : string) =
        if String.length line > 0 then
          let c = line.[0] in
          let qline = eqline (c = '=') line in
          let asplit = Str.split spacereg qline in
          let el2 = List.nth asplit 2 in
          let el3orfree = List.nth asplit 3 in
          let el3 = if el3orfree = "free" then "-" else el3orfree in
          let d = "/dev/" ^ el2 in
          let aname = ":" ^ ismin (el2 = "-") d in
          let amount = ":" ^ mountval (mylookupkv d !dev2mount) d in
          let atype = ":" ^ el3 in
          let ro = ":" ^ roval (mylookupkv d !dev2ro) d in
          let openreg = Str.regexp "(" in
          let closereg = Str.regexp ")" in
          let openpos = regexpos openreg qline in
          let closepos = regexpos closereg qline in
          if openpos <> -1 then
            if closepos <> -1 then
              let asize = String.sub qline openpos (closepos - openpos + 1) in
              let output =
                  padme 19 aname ^ padme 13 asize ^ padme 20 atype
                  ^ padme 20 amount ^ padme 11 ro ^ "\n"
              in
              print_string output
            else ()
          else ()
    in
    let glabeltxt_to_kv (lines : string list) = List.map glabelline lines in
    let glabeltxt : string list = read_process_lines "glabel status | awk '{if (NR!=1) print $1,$3}'" in
    let mounttxt_to_kv (lines : string list) = List.map mountline lines in
    let mounttxt : string list = read_process_lines "mount" in
    let ntfstxt_to_kv (lines : string list) = List.map ntfsline lines in
    let ntfstxt : string list = read_process_lines "pgrep ntfs-3g" in
    let gparttxt_to_kv (lines : string list) = List.map gpartline lines in
    let gparttxt : string list = read_process_lines "gpart  show -p" in
    let (_ : unit list) = glabeltxt_to_kv glabeltxt in
    let (_ : unit list) = mounttxt_to_kv mounttxt in
    let (_ : unit list) = ntfstxt_to_kv ntfstxt in
    let (_ : unit list) = gparttxt_to_kv gparttxt in
    ()
