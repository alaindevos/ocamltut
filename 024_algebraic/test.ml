(* Algebraic data types **********************************)
type blue_or_pink_int =
    | Blue of int
    | Pink of int
let a:blue_or_pink_int = Blue(20);;
let b:blue_or_pink_int = Pink(20);;
