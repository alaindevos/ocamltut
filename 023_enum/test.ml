(* Enum & algebraic data types ************************************)
type myenum =
    | Man
    | Women
    | Its;;

type algebraic =
    | Couple of int * int
    | Triple of int * int * int;;

let a : myenum = Man ;;
let b : algebraic = Couple(50,20) ;;
let c : algebraic = Triple(50,20,10) ;;
() ;;
