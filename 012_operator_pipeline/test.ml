(* Operator pipeline ************************************************************)
print_int ( ( + ) 1 2 );;
print_int ( ( * ) 1 2 );;
print_string ( Bool.to_string (( = ) 1 2 ));;
let ( <^> ) (x:'a) (y:'a) = ((max x y):'a);;
print_int ( 1 <^> 2);;
(* application *)
let ( @@ ) f x = f x;;
print_int( succ @@ 2 * 10);;
(* reverse application or pipeline *)
let ( |> ) x f = f x;;
let square x = x * x ;;
print_int ( 3 |> succ |> square );;
