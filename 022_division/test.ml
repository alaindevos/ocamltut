(* Safeint & nil ******************************************************)
type safeint =
    | Int of int
    | Nil
let _=
    let myprintint (s:string) (x:int)=
        let _ = print_string (s^":") in
        let _ = print_int x in
        let _ = print_newline () in
        () in
    let safediv1 (x:int) (y:int) : safeint =
        if (y<>0) then (Int (x/y))
                 else  Nil in
    let safediv2 (x:safeint) (y:safeint) : safeint = match y with
        | Int b -> begin
                        match x with
                            | Int a -> (safediv1 a b)
                            | Nil   -> Nil
                   end
        | Nil   -> Nil in
    let printsafeint (s:string) (x:safeint) : unit = match x with
        | Int a -> (myprintint s a)
        | Nil   -> (myprintint "Nil" (-1))
                   in
    (* Calculate safe integer ((a/b) / (c/d)) *)
    let myfunction (a:int)(b:int)(c:int)(d:int)=
       let x:safeint=safediv1 a b in
       let y:safeint=safediv1 c d in
       let z:safeint=safediv2 x y in
       z in
    let _= printsafeint "Result" (myfunction 256 16 16 4 ) in
    ()
