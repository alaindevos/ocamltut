open Unix 


let myfun=fun() ->
    let counter = ref 0 in 
    while true do
        sleep 1;
        counter := !counter + 1;
        print_int !counter;
        print_endline ""
    done

let (_:unit)=
    let [@warning "-26"] t1=Thread.create myfun () in
    let [@warning "-26"] t2=Thread.create myfun () in 
    sleep 100;
    ()
