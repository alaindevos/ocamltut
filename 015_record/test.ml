(* order is irrelevant ****************************************************)
type student = {
    name:string;
    year:int;
}

let alain:student={
    name="Alain";
    year=1954;
};;
print_string alain.name;;
(* Error : let alain.name="Eddy";; *)

(*copy*)
let eddy:student={alain with name="Eddy"};;
