(* for and while loop ***************************************)
let ()=
    for i = 1 to 5 do
        print_int i
    done;
    let i=ref 1 in
    while (!i <=5) do
        print_int !i;
        i:=!i+1
    done;
    ();;
    
