(* read file line by line *********************************)
let print_file channel =
    let t = ref 0 in
    let rec pf channel =
        let ilc = input_line channel in 
        match ilc with
        | line ->
            incr t ;
            Printf.printf "%d:" !t ;
            print_endline line ;
            pf channel
        | exception End_of_file -> 
            () 
        in
    pf channel 
    in
let fn = "file.txt" in
let ic = open_in fn in
    try 
        print_file ic ;
        close_in ic                 (* close the input channel *) 
    with e ->                       (* some unexpected exception occurs *)
        close_in_noerr ic ;         (* emergency closing *)
        raise e ;;                  (* exit with error: files are closed but channels are not flushed *)

