(* Optional argument ****************************************)
let _ =
    let myplus  ?x:(xx=8) ~y:yy () = xx+yy in
    let asum = myplus ~y:7 () in
    print_int asum
