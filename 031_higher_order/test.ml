(* higher_order_function =  function returning a function ********************************)
let mydouble x=2*x;;
let applytwice  (f:int->int) (x:int):int = f ( f x);;
let applytwice2 (f:int->int) (x:int):int = x |> f |> f ;;
let quad x  = applytwice mydouble x;;
let quad2   = applytwice mydouble;;

let rec addonetoalist = function
    | []   -> []
    | h::t -> (h+1):: (addonetoalist t) ;;

(* implement map *)
let rec transform f = function
    | []   -> []
    | h::t -> (f h):: (transform f t);;

let adonebis lst= transform (fun x-> x+1) lst;;

let stringlist_of_intlist lst  = transform (fun x -> (string_of_int x)) lst;;
let stringlist_of_intlist2 lst = transform string_of_int lst;;
