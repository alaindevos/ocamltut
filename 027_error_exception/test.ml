(*exceptions are variants ************************************)
exception Abadthing
exception Foo of string
let f=Foo "oops";;
raise f;;
raise (Failure "my error message");;
failwith "my error message";;

let savediv x y=
    try x / y with
    | Division_by_zero -> 0;;
let i=savediv 1 0 ;;

