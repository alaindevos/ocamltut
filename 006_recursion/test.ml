(* recursion ********************************************************************)
let rec (fact : int -> int) =
 fun (n : int) : int ->
  (if (n : int) = (0 : int) then (1 : int) else n * fact (n - 1) : int)
in
print_int (fact (4 : int))
