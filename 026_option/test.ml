(* int option ***************************************************)
type toption =
    | None
    | Some of int;;

let print_toption t=match t with
    | None -> 0
    | Some x -> x;;

let rec lookup al x=match al with
    | [] -> None
    | h::t -> if (x=h) then Some h
                       else lookup t x;;
let z=lookup [1;2;3] 2;;

Printf.printf "%d\n" (print_toption (Some 3));;


(* int list *****************************************************)
type tlist =
    | Nil
    | Cons of int * tlist;;

let a1=Nil;;

let a2=Cons (1,a1);;

let a3=Cons (2,a2);;

let rec mylen al=match al with
    | Nil -> 0
    | Cons (_,t) -> 1+mylen t;;

let rec myappend al x=match al with
    | Nil -> x
    | Cons (h,t) -> Cons (h, myappend t x);;

Printf.printf "%d\n" (mylen a3);;
