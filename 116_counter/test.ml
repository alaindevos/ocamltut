class counter = object
    val  mutable n = 0
    method incr =
        n <- n + 1
    method get =
        n
    method print =
        print_int n
end

let _ =
    let c = new counter in
    c#incr;
    c#incr;
    c#print;
    ()
