(* Run a command and return its results as a list of strings,
   one per line. *)
let read_process_lines command =
  let lines = ref [] in
  let in_channel = Unix.open_process_in command in
  begin
    try
      while true do
        lines := input_line in_channel :: !lines
      done;
    with End_of_file ->
      ignore (Unix.close_process_in in_channel)
  end;
  List.rev !lines

let printhelpers x = print_string x;
					 print_string "::";;

let stringlistprint2 lst=List.map printhelpers lst ;;

let stringlistprint lst=
		ignore (stringlistprint2 lst);
		print_string "[]";
		print_newline ();;

(* Example: *)
let output_lines = read_process_lines "ls -al";;
stringlistprint output_lines;;
