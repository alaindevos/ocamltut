let read_process_lines command =
  let lines = ref [] in
  let in_channel = Unix.open_process_in command in
  begin
    try
      while true do
        lines := input_line in_channel :: !lines
      done;
    with End_of_file ->
      ignore (Unix.close_process_in in_channel)
  end;
  List.rev !lines

(* implicit second parameter with case*)
let rec lookupkv ak = function
	| [] -> ""
	| (k,v)::t -> if k=ak then v 
						  else lookupkv ak t;;
let regexpos regex line=
	try Str.search_forward regex line 0
	with Not_found -> -1;;

let spacereg=Str.regexp " +";;

let nam2dev=ref [] ;;
let dev2nam=ref [] ;;
let glabelline line=
		let lst=Str.split spacereg line in
		let n="/dev/" ^(List.hd lst) in
		let d="/dev/" ^(List.hd (List.tl lst)) in
		nam2dev:= (n,d) :: !nam2dev;
		dev2nam:= (d,n) :: !dev2nam;;
let glabeltxt_to_kv lines=
		List.map glabelline lines;;
let glabeltxt = read_process_lines "glabel status | awk '{if (NR!=1) print $1,$3}'";;
glabeltxt_to_kv glabeltxt;;

let dev2mount=ref [] ;;
let dev2ro=ref [] ;;
let mountline line=
		let lst=Str.split spacereg line in
		let w=(List.hd lst) in
		let t=(List.hd (List.tl (List.tl lst))) in
		let v=lookupkv w !nam2dev in
		let w2= if  v<>"" then lookupkv w !nam2dev
			              else w in
		dev2mount:= (w2,t) :: !dev2mount ;
		let roreg=Str.regexp "read-only" in
		let pos=regexpos roreg line in
		if  pos <> -1 then dev2ro:=(w2,"ro") :: !dev2ro
					  else dev2ro:=(w2,"rw") :: !dev2ro ;;
let mounttxt_to_kv lines= List.map mountline lines;;
let mounttxt = read_process_lines "mount | grep -e ^/dev/" ;;
mounttxt_to_kv mounttxt;;

let id_to_command cid="ps -p " ^ cid ^ " -o command | sed 1d | sort -u" ;;(*Vermaden*) 
let ntfsline line=
		let lst=Str.split spacereg line in
		let id=List.hd lst in
		let command= id_to_command id in
		let pstxt=read_process_lines command in
		let lst2=Str.split spacereg (List.hd pstxt) in
		let d=List.hd (List.tl (List.tl (List.tl lst2))) in
		let m=List.hd (List.tl (List.tl (List.tl (List.tl lst2)))) in
		dev2ro:=(d,"?") :: !dev2ro ;
		dev2mount:= (d,m) :: !dev2mount ;;
let ntfstxt_to_kv lines=
		List.map ntfsline lines;;
let ntfstxt = read_process_lines "pgrep ntfs-3g" ;; (*Vermaden*)
ntfstxt_to_kv ntfstxt;;

let eqline x line=
			let len=String.length line in
			if x then String.sub line 2 (len-2)
				 else line;;
let ismin x line=if x then "Free" else line ;;
let mountval x d=if x="" then "-" else lookupkv d !dev2mount;;
let roval x d   =if x="" then "-" else lookupkv d !dev2ro;;
let padme x s =let numspaces=x - (String.length s) in
			   let spacesref=ref "" in
			   for i=0 to numspaces do
					spacesref:=" "^ (!spacesref)
			   done;
               let sum=s^ (!spacesref) in
               sum ;;
let gpartline line=
		if (String.length line)>0 then
			let c=line.[0] in
			let qline=eqline (c='=') line in
			let asplit=Str.split spacereg qline in
			let el2=List.hd (List.tl (List.tl asplit)) in
			let el3=List.hd (List.tl (List.tl (List.tl asplit))) in
			let d="/dev/"^el2 in
			let aname= "Name:" ^(ismin (el2 = "-") d) in
			let amount="Mount:"^(mountval (lookupkv d !dev2mount) d) in
			let atype="Type:"^el3 in
		    let ro="Option:"^(roval (lookupkv d !dev2ro) d) in
	        let openreg=Str.regexp "(" in
	        let closereg=Str.regexp ")" in
	        let openpos=regexpos openreg qline in
			let closepos=regexpos closereg qline in
			if openpos<> -1 then
				if closepos <> -1 then
					let asize=String.sub qline openpos (closepos-openpos+1) in
					let output=(padme 19 aname)^(padme 13 asize)
					          ^(padme 11 ro)^(padme 20 atype)^amount^"\n" in
					print_string output;;
let gparttxt_to_kv lines=
		List.map gpartline lines;;
let gparttxt = read_process_lines "gpart  show -p" ;;
gparttxt_to_kv gparttxt;;

