open Core
open Option
(*Association list*)
let digits=
	[(0,"nul");
	 (1,"een");
	 (2,"twee")];;

let x=List.Assoc.find digits 1 ~equal:(=);;
let string_of_stringoption = function None -> "" | Some n -> n
let y=string_of_stringoption x;;
print_string y;
