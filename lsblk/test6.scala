#!/usr/bin/env scala
import scala.sys.process._
import scala.collection.mutable.HashMap
import scala.util.matching.Regex

val glabeltxt=("tcsh -c \"  glabel status | awk '{if (NR!=1) print $1,$3}' \" ").!!
val nam2dev: HashMap[String, String] = HashMap()
val dev2nam: HashMap[String, String] = HashMap()
val glabelarr=glabeltxt.split("\n")
for (line <- glabelarr) {
	val asplit=line.split(" ")
	val n="/dev/"+asplit(0)
    val d="/dev/"+asplit(1)
    nam2dev+=(n->d)
    dev2nam+=(d->n)
}

val mounttxt=("tcsh -c \" mount | grep -e ^/dev/ \" ").!!
val dev2mount: HashMap[String, String] = HashMap()
val dev2ro: HashMap[String, String] = HashMap()
val mountarr=mounttxt.split("\n")
for (line <-mountarr){
	val asplit=line.split(" ")
    var w=asplit(0)	
    if (nam2dev.contains(w)) {w=nam2dev(w)}
    dev2mount(w)=asplit(2)
 	dev2ro(w)="rw"
 	val pattern = "read-only".r
	if (pattern.findFirstMatchIn(line).isDefined) {dev2ro(w)="ro"}
}

//Vermadensline
val ntfsidsstr=("tcsh -c \" pgrep ntfs-3g \" ").!!
val ntfsidsarr=ntfsidsstr.split("\n")
for (cid <-ntfsidsarr){
    //Vermadensline
    val todo=" \" ps -p "+cid+" -o command | sed 1d | sort -u \" "
    val output=("tcsh -c "+todo).!!
    val asplit=output.split(" ")
    var d=asplit(3)
    var m=asplit(4)
    dev2ro(d)="?"
    dev2mount(d)=m
}


val gparttxt=("tcsh -c \" gpart  show -p \" ").!!
val gpartarr=gparttxt.split("\n")

for (line <-gpartarr){
	var aline=line.trim
	if(line.length>0){
		if (line.charAt(0)=='=')  {
			println("++++++++++++++++++++++++++")
			aline=aline.substring(2,line.length)
		}
		aline=aline.trim
		val asplit=aline.split(" +")
		var d="/dev/"+asplit(2)
		if (asplit(2)=="-") {d="Free"}
		var aname="Name:"+d
		var amount="-"
		if (dev2mount.contains(d)){amount=dev2mount(d)}
		amount="Mount:"+amount
		var atype=asplit(3)
		atype="Type:"+atype
		var ro="-"
		if (dev2ro.contains(d)){ro=dev2ro(d)}
		ro="Option:"+ro
		val reg="""(?<=\()[^)]+(?=\))""".r
		var asize="Size:("+reg.findFirstMatchIn(aline).getOrElse("unknown")+")"
		val anamespace=" "*(19-aname.length)
		val asizespace=" "*(13-asize.length)
		val rospace   =" "*(11-ro.length)
		val atypespace=" "*(20-atype.length)
		var output=""
		output=output+aname+anamespace
		output=output+asize+asizespace
		output=output+ro+rospace
		output=output+atype+atypespace
		output=output+amount
		println(output)
	}
}
