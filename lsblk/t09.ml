(*no null*)
let printint p=
		print_int p;
		Printf.printf "\n";;
let printstring p=
		print_string p;
		print_newline ();;
let printfloat p=
		print_float p;
		print_newline ();;
let printchar p=
		print_char p;
		print_newline ();;
let printbool p=
		Printf.printf "%B \n" p ;;
		
printstring "Let's start";;

(*Consts*)
let x=1+2;;                (*const*)
let y=ref 3;;              (*var*)
printint !y;;             (*3*)
y:=!y+1;;
printint !y;;              (*4*)
(*Scope*)
let x=20;;           
let x=10 in 
	printint x ;;           (*10*)
printint x;;                (*20*)
(*Control structure*)
let atest ()=if 1=1 
				then 1 
				else 0;;
printint (atest ());;        (*1*)
let btest ()=if 1<>1 
				then 1 
				else 0;;
printint (btest ());;        (*0*)
let c ()= begin
			printint 1 ;
			printint 2 ;
			printint 3
		  end;;
c ();;        (*123*)
let d ()= (
			printint 1 ;
			printint 2 ;
			printint 3
		  );;
d ();;        (*123*)

(**for**)
for i= 1 to 10 do 
	printint i 
done;;                       (*1 ... 10*)

let n=10;;
let sum=ref 0;;
for i=1 to n do
	sum:=!sum + i
done;;
printint !sum;;              (*55*)

(**while**)
let n=10;;
let sum=ref 0;;
let i=ref 1;;
while !i <= n do
	sum:=!sum + !i;
	i:=!i + 1
done;;
printint !sum;;              (*55*)

(*Procedures*)
let x=ref 10 ;;
let setx p=x:=p ;;         (* a procedure returns ()*)
setx 5;;
printint !x;;               (*5*)
let set3 ()=x:=3;;        (* argument is () *)
set3 ();;
printint !x;;               (*3*)

(*Functions*)
let aprint ()=
	printstring "a";
	printstring "b";
	printstring "c";;
aprint ();;	                
let plus x y=x+y;;
printint (plus 2 3);;       (*5*)
let quad x=
	let sqr x=x*x in
		(sqr x)*(sqr x);;
printint (quad 2);;         (*16*)
let add1= fun x -> x+1;;
printint (add1 2);;         (*3*)
let apply afun p= afun p;;
apply printint 2            (*2*)
let rec printdown i=
	if i>0  then 
				(printint i;printdown (i-1))
			else
				();;
printdown 5;;               (*5...1*)
let f x=x;;
printint(f 3);;             (*3*)
printstring(f "ab");;       (*"ab"*)

(*basic types*)
(**int**)
let f=float_of_int  12;;
(**float**)
let i=int_of_float  123.45;;
let f= 1.0 /. 3.0;;
let pi = 4. *. atan 1.0;;
let x=sin (pi /. 5.0);;
(**Strings**)
let i=int_of_string "123";;
let f=float_of_string "123";;
let s1="abc";;
let s2="def";;
let s3=s1^s2;;
print_string s3;;          (*abcdef*)
let c=s3.[0];;
printchar c;;              (*a*)
Bytes.set s3 0 'x';;
printchar s3.[0];;         (*x*)
let len=String.length s3;;
printint len;;                   (*6*)
printchar s3.[(String.length s3) - 1];; (*f*)
let subs=String.sub s3 2 4;;
printstring subs;;              (*cdef*)
(***String traversal***)
let rec traverse_print s =
	print_char s.[0]; 
	print_newline ();
	if (String.length s)=1
		then ()
	else 
		traverse_print (String.sub s 1 ((String.length s) - 1));;
traverse_print s3;;
let isequal () =
	if "aladin"="aladin" then printstring "equal";;
isequal ();;

(*Types*)

(*Enumerated types*)

type enum = A | B | C ;; 
let a=A;;
let myok= if (A < B)
					then
						"OK\n"
					else
						"not OK\n";;
print_string myok;;
print_string "-----HIER------------";

(*Aggregate types*)

type agg= Agg of int * string ;;
let a= Agg (1,"hi");

type btnode =
	| Leaf
	| Node of string * btnode * btnode ;;

(*
A binary tree node is either a leaf of the tree, 
which has no fields, 
or a node, which has three fields: 
a string, a binary tree node, and another binary tree node. 
*)


type treex = Empty | Node of int * treex * treex ;;

type tree = Tree of int * tree | Leaf of int ;;
let t = Tree (3, Tree( 2, Leaf 1));;

type bitree = Tree of int * tree * tree | Leaf of int | Null;;

type mtree = Tree of int * mtree list | Leaf of int | Null;;
let mt = Tree (3, [Tree (2, [Null]); Tree (2, [Tree (1, [Leaf 1])])]);;

(*Records*)                 (*can have mutable fields, example : a reference *)
type identity= {firstName :string;
				age: int
};;

type complex={ re:float ; im:float };;
let x= {re=1.0;im=2.0};;
printfloat x.re ;;          (*1. *)
type person={ name:string; mutable age:int }
let p ={ name = "Martin" ; age = 23 };;
p.age<-p.age+1;;
printint p.age              (*24*)

(*Tuples*)              
let v=(1,true,"hello",'a');;
let (a,b,c,d)=v in printstring(c);;
let muldiv x y = (x*y,x/y);;     (*example*)
let sum (x,y) = x+y ;;
printint (sum (3,5));;      (*8*)

let thirdelementoftuple tup= match tup with (_,_,x) -> x;;
let res=thirdelementoftuple (1,"Hallo","Ja");;
printstring res;;                           
let a=1;;
let b=2;;
let (a,b)=(b,a) ;;         (*swap*)
let divmod (a,b)= (a/b,a mod b);;

(*Arrays*)                  (*like lists but, homogeneous,mutable,and you can select any element,objects*)
(**How to print array**)
let arrayofintprinthelper arr=
		Array.iter (fun el -> print_int el; print_string ";" ) arr;;
let arrayofintprint arr=
		print_string "[|";
		arrayofintprinthelper arr;
		print_string "|]";
		print_newline ();;
		
let a=[|2;4;6;8|];;          
arrayofintprint a;;
let a=Array.make 10 0;;     (*[|0;0;0;0;0;0;0;0;0;0;0|]*)
let a=Array.init 5 (fun i -> i*i);;  (*[|0;1;4;9;16|] *)
printint a.(3);;              (*9*)
printint (Array.get a 3);;      (*9*)      
printint (Array.length a);;   (*5*)
let lst=Array.to_list a;;     (*conversion*)
let a=[|1;2;3;4;5;6;7;8;9;10|];;          
Array.set a 3 10 ;;           (*set value*)
a.(3) <- -4 ;;                (*set value*)
arrayofintprint a;;
let suba=Array.sub a 2 3;;    (* 3,-4,5 *)
arrayofintprint suba;;
let a1=[|1;2|];;
let a2=[|3;4|];;
let a=Array.append a1 a2 ;;
arrayofintprint a;;           (* 1,2,3,4 *)
(**Array iteration **)
let arr=[|1;2;3;4|];;
let afun=fun el -> print_int el;print_string "," ;;
Array.iter afun arr;;         (*1,2,3,4*)
print_newline ();;
let b=Array.map (fun el -> 2*el) a;;
arrayofintprint b;;           (*2,4,6,8*)
let res=Array.fold_left (fun el1 el2 -> el1+el2) 0 a;;
printint res ;;               (*10*)
(**Array sorting **)
let arr=[|-1;5;2;4|];;
Array.sort compare arr;;
arrayofintprint arr;;         (* sorted *)
(**Array traversal **)
let maxarr arr=
	let curmax = ref arr.(0) in
	let i = ref 1 in
		while !i < Array.length arr do
			if (arr.(!i) > (!curmax)) 
				then 
					curmax := arr.(!i);
					i := !i + 1
		done;
		!curmax;;
let ar r=[|-1;5;2;4|];;
let res=maxarr arr;;
printint res;;               (*5*)

(*Hashtables*)               (*Like an array *)
let h=Hashtbl.create 0;;    
Hashtbl.add h "aa" "bb";;
let res=Hashtbl.find h "aa";;
printstring res ;;           (*bb*)
Hashtbl.replace h "aa" "cc";;
let res=Hashtbl.find h "aa";;
printstring res ;;           (*cc*)
Hashtbl.remove h "aa";;
let h=Hashtbl.create 0;;    
Hashtbl.add h "aa" "bb";;
Hashtbl.add h "aa" "cc";;
let f=Hashtbl.find h "aa";;
printstring f ;;              (*cc*)
let fa=Hashtbl.find_all h "aa";;
let head1=List.hd fa;;
printstring head1;;           (*cc*)
let tail1=List.tl fa;;
let head2=List.hd tail1;;
printstring head2;;           (*bb*)

let s=Hashtbl.length h;;
printint s;;                   (*2*)

(*
stringlistprint res;;          ("cc"::"bb")
*)
printstring "Beginhash------------------";;
(**Hash interation**)
let hfun key value=
		print_string key ;
		print_string ":";
		print_string value;
		print_newline ();;

let histprint htbl =
	Hashtbl.iter hfun htbl;;

let h=Hashtbl.create 0;;    
Hashtbl.add h "aa" "bb";;
Hashtbl.add h "cc" "dd";;
histprint h;;
printstring "Endhash------------------";;

(**Hash reverse lookup value -> key**)
let reverse_lookup tbl v =
	let lst = ref [] in
		Hashtbl.iter (fun key value -> 
						if value==v then lst := key::!lst ) tbl; 
	lst;;

(*Lists*)                   (*homogeneous*)
let printhelperi x = print_int x;
					 print_string "::";;


let intlistprint2 lst=List.map printhelperi lst ;;

let intlistprint lst=
		ignore (intlistprint2 lst);
		print_string "[]";
		print_newline ();;

let printhelpers x = print_string x;
					 print_string "::";;

let stringlistprint2 lst=List.map printhelpers lst ;;

let stringlistprint lst=
		ignore (stringlistprint2 lst);
		print_string "[]";
		print_newline ();;

intlistprint [1;2;3;4];;
let l=1::2::3::[];;
let l=[1;2;3];;
let head=List.hd l;;
let tail=List.tl l;;
printint head;;             (*1*)
intlistprint tail;;         (*23*)
printint (List.nth l 2);;   (*3*)
let concat=l@l;;           
intlistprint concat;;       (*123123*)
let reverse=List.rev l;;         (*reverse*)
intlistprint reverse;;       (*321*)
List.iter printint l;;     (*iterate=123*)
let res=                             (*new list*)
		let add10 p = p+10 in 
			List.map add10 l;;       (*add 10 to each element*)
intlistprint res;;                   
let sum= List.fold_left (+) 0 l;;
printint sum;;              (*1+2+3=6*)
let arr=Array.of_list concat;;     (*conversion*)

let a=["a";"b";"c"];;
let b=[1;2;3];;
let res=List.combine a b ;;
let (x,y)=List.hd res ;;
printint y;;                         (*1*)

let print_tuple tup =
	let s, n = tup in
		print_string "(";
		print_string s;
		print_string ", ";
		print_int n;
		print_string")";
		print_newline ();;

let rec tuplister tlst = 
	match tlst with
		| [] -> ()
		| h::t -> print_tuple h;tuplister t;;

tuplister res;;

(**List traverse**)
let rec sum x =
	match x with
	|	[]->0
	|   h::t-> h+(sum t);;
printint(sum [1;2;3]);;     (*6*)   
let rec append x y =
	match x with
	|	[]   -> y
	|   h::t -> h:: append t y;;
intlistprint (append [1;2] [3;4]);;  (*1234*)

(**List sort**)
let lst=[7;-3;5];;
let sort_help e1 e2 = if e1>e2
							then 1
					  else if e1=e2
							then 0
					  else 
							    -1;;
let sorter alst = List.sort sort_help alst;;
intlistprint (sorter lst);;         (*-357*)
let sorter alst = List.sort (fun e1 e2 -> e1-e2) alst;;
intlistprint (sorter lst);;         (*-357*)

(**List recursion**)
let rec arange m n accum =
			if m=n
				then 
					n::accum
				else 
					(arange m (n-1) (n::accum));;

intlistprint (arange 5 8 [] ) ;;    (*5678*)   

let rec sum_help lst accum =
			match lst with
				| [] -> accum
				| h::t -> sum_help t (h+accum);;
let summer lst = sum_help lst 0;;

printint ( summer [2;3;4]);;        (*9*)


(*Association list*)
let digits=
	[(0,"nul");
	 (1,"een");
	 (2,"twee")];;
(*
let x=List.Assoc.find digits 1;;
printstring x;;
*)
printstring "---------------";;

(*Objects*)

(*Pattern matching*)
let mynot x =
	match x with
	|	1 ->  0
	|   0 ->  1
	|   _ as t -> -t;;       (* inverse the others *)
printint (mynot 0);; (*1*)

(*Regular expressions*)
(**find**)
let r=Str.quote "a+";;       (* string rep of search  a+*)      
print_string (r^"\n") ;;            (*  a\+      *)
let r=Str.regexp "ala";;     (*  search aaa *)
let s="sss balab ttt";;
let b=Str.string_match r s 1;;
printbool b;;                (* false : at pos 1 there is no ala !! *)
let b=Str.string_match r s 5;;
printbool b;;                (* true *)
(**replace**)
let i=Str.regexp "aa";;
let o="xxx";;
let s="tataataaataaaa";;
let res=Str.global_replace i o s;;
printstring res;;            (*tatxxxtxxxatxxxxxx*)
(**splitting**)
let s= "This is a  test";;
let r=Str.regexp " +";;
let lst=Str.split r s;;
stringlistprint lst;;

(*I/O*)
(*
let input= read_line ();;
*)
printstring "Done :-------------\n"
