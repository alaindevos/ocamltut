(* Write to files *************************************)
let ()=
    let oc=open_out "myfile.txt" in
    try
        Printf.fprintf oc "Hallo";
        close_out oc
    with e->
        close_out oc;
        raise e;;
