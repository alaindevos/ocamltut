(* tail recursive list length **********************************)
type intlist=int list 
let fmakelist (n:int):intlist=List.init n (fun x->x) ;;
let biglist:intlist=fmakelist 1000000 ;;

let rec flistlength (n:int) (lst:intlist):int =
    match lst with
    | [] -> n
    | h::t -> flistlength (n+1) t;;

print_int (flistlength 0 biglist);;


