(* char & string & index **********************************************************)
let _=
    let s="abcdef" in
    let c=s.[2] in
    Printf.printf "\n %c \n" c
