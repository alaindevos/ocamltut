(* Lazy fibonacci****************************************************)
type 'a myseq =
  | Cons of 'a * (unit -> 'a myseq)
  | Nil
type intseq = int myseq
let _=
    let rec mysum (z:intseq):int = match z with
        | Cons (hd, tl) -> hd + (mysum (tl ()))
        | Nil -> 0 in
    let rec mytake (n:int) (s:intseq): int list =
        if n <> 0 then
            match s with
                | Cons(hd,tl) -> hd :: (mytake (n - 1) (tl()))
                | Nil -> []
            else [] in
    let rec myprintlist (al: int list): unit = match al with
        | [] -> ()
        | hd::tl -> print_int hd;
                    print_newline();
                    myprintlist tl in

    (* a manual sequence *)
    let l1=Cons (1,fun() -> Nil) in
    let l2=Cons (2,fun() -> l1) in
    let l3=Cons (3,fun() -> l2) in
    let _= myprintlist (mytake 3 l3) in
    let _= print_int (mysum l3) in

    (* sequence of natural numbers *)
    let rec natnumseq (n:int):intseq = Cons (n, fun () -> natnumseq (n + 1)) in
    let nats:intseq = natnumseq 0 in
    let _= myprintlist (mytake 10 nats) in

    (* fibonacci sequence *)
    let rec fibseq (a:int)(b:int):intseq =
        Cons (a+b,fun()-> fibseq b (a+b)) in
    let fibs:intseq=fibseq 1 1 in
    let _= myprintlist (mytake 10 fibs) in
    ()

