(*General assocation list *******************************************)
let fst p = match p with
    (x  , _) -> x;;
let snd p = match p with
    (_  , y) -> y;;
let rec mylookup al x=match al with
    | [] -> raise Not_found
    | (k,v)::t -> if (k=x) then v
                          else mylookup t x;;
let rec myadd al k v=match al with
    | [] -> [(k,v)]
    | (k2,v2)::t -> if (k=k2) then (k,v)::t
    			      else (k2,v2):: myadd t k v;;
let rec myremove al k = match al with
    | [] -> []
    | (k2,v2)::t -> if (k=k2) then t
                              else (k2,v2):: myremove t k;;

(*string-int association list ****************************************************) 
let dict = ["foo", 5; "bar", 10; "baz", 15] ;;
let bar_num = try List.assoc "bar" dict with Not_found -> 0;;
print_endline (if List.mem_assoc "foo" dict then "key found" else "key missing");;
Printf.printf "%s\n" (fst ("a","b") );;
Printf.printf "%s\n" (snd ("a","b") );;
Printf.printf "%s\n" (mylookup  [("a","b") ; ("c","d")] "c");;

(*string-string assocation list **************************************************)
let sum =myadd [("a","b") ; ("c","d")] "e" "f";;
let t=snd (List.hd(List.tl(List.tl sum)));;
Printf.printf "%s\n" t ;;
let r =myremove [("a","b") ; ("c","d")] "c";;
let rr=fst (List.hd r);;
Printf.printf "%s\n" rr ;;
