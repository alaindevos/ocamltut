(* module include *****************************)
module Amodule=struct
    let hello () = print_endline "Hello"
end

module Bmodule=struct
    include Amodule
    let hello2()=Amodule.hello ()
end

let () = Bmodule.hello2 ()
