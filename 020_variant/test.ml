(* enumerate constants *******************************************************)
type intorfloat = MyInt of int | MyFloat of float 
type color=Red|Green|Blue
let c:color=Red;;

type point=float*float
type shape=
    | Circle of {center:point;radius:float}
    | Rectangle of {lowerleft:point;upperright:point}
let c1:shape=Circle{center=(0.,0.);radius=1.};;
let r1:shape=Rectangle{lowerleft=(0.,0.);upperright=(1.,1.0)};;

let avg a b = (a+.b)/. 2.;;

let center (s:shape):point=
    match s with
    | Circle {center;radius}->center
    | Rectangle {lowerleft;upperright} ->
            let (xll,yll)=lowerleft in
            let (xur,yur)=upperright in
            (avg xll xur,avg yll yur);;

(* Option *)
let divide x y = 
    if y=0  then None
            else Some (x/y);;
