open Array 
let ()=
    let printar a= 
        let rec helper i=
            print_int a.(i);
            if i+1 >= length a then () 
            else helper (i+1) in 
        helper 0 in 
    let myswap a x y = 
        let t=a.(x) in 
        a.(x)<- a.(y);  
        a.(y)<- t in 
    let a= [|1;2;3;4;5;6;7;8;9;10|] in
    myswap a (4-1) (8-1); 
    printar a; 
    ()

