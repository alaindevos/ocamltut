
type state={mutable count:int}

type counter = {
    incr:unit->unit;
    decr:unit->unit;
    reset:unit->unit;
    get:unit->int;
}

let mk_counter(_:unit):counter=
    let s:state={count=0} in 
    {
        incr= ( fun() -> 
                    s.count<-s.count+1 );
        decr= ( fun() -> 
                    s.count<-s.count-1);
        reset=( fun() ->
                    s.count<-0);
        get=  ( fun() ->
                    s.count);
    }

let main()=
    let myc:counter=mk_counter () in 
    myc.incr ();
    myc.incr ();
    print_int (myc.get ())

let _=main()

