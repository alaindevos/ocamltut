(* Replace type constructor by Nil punctuation [] *)
(* And type constructor Cons by prefix operator (::) *) 

type 'a alist = [] | ( :: ) of 'a * 'a alist

let (_ : unit) =
  let rec mylength (lst : 'a alist) : int =
      match lst with [] -> 0 | ( :: ) (h, t) -> 1 + mylength t
  in

  let rec ( @ ) (lst1:('a alist)) (lst2:('a alist)):('a alist) = match lst1 with
    | [] -> lst2 
    | (::) (h,t) -> ( :: ) (h,( ( @ ) t lst2)) in 

  (* a  list of int containint  1 and 2 *)
  let alist : int alist = ( :: ) (1, ( :: ) (2, [])) in
  let two: int alist= ( @ ) alist alist in 
  let alen : int = mylength two in
  print_int alen

