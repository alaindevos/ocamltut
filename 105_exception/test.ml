(* Build in (variant) type of exceptions is "exn" *)
(* All exceptions are values of type exn *)
exception MyInvalid 
exception MyFailure of string
let die:exn=MyFailure "My division by zero";;
(*
let _:'a=raise die;;

Note : Convienence function : failwith "blabla" === raise (Failure "blabla")  
*)

let savediv (x:int) (y:int):int=
(*try is like match but for exceptions *)
    try 
        if (x=0) then 
            raise (MyFailure "\nZero is to small to divid even by zero\n")
        else 
            x/y
    with
        | Division_by_zero -> 0
        | MyFailure m -> print_string m;
                         -1
        | _ -> -2       (* E.g. another problem during x/y *)


let ()=print_int (savediv 0 0);
    print_endline "";
    print_int(savediv 1 0)

