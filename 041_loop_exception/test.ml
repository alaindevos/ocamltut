(* Loop exception *********************************************)
let () =
    let exception IntEx of int in
    try
      for i = 0 to 10 do
        let () = print_int i in
        if i = 5 then raise (IntEx i) else ()
      done;
      ()
    with IntEx i -> 
        let ()=print_int  i  in
        () 

(* Loop exception ***********************************************)
exception Break
let () =
    let ()=Printf.printf "Start" in 
    let ()=
        try
            for i = 0 to 9 do 
                if i = 4 then 
                    raise Break 
                else 
                    Printf.printf "%d\n%!" i
            done 
        with
        | Break -> let ()=Printf.printf "Im break" in 
        let ()=Printf.printf "Stop" in 
        () in 
    let ()=Printf.printf "Done" in 
    ()


