let f () =
    let exception IntEx of int in
    try
      for i = 0 to 10 do
        let () = print_int i in
        if i = 5 then raise IntEx i else ()
      done;
      ()
    with IntEx i -> print_string (Printexc.to_string i) ()
