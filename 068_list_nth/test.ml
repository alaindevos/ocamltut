( nth element of a list ***************************************)
type somestring= None | Some of string
type stringlist=string list 

let fgetstring (x:somestring):string = match x with
    | None -> "None"
    | Some x -> x;;

let fprint (x:somestring)=
    let y=fgetstring x in 
    let ()=print_string y in
    ();;

let rec fgetnth (n:int) (lst:stringlist):somestring =match lst with
    | [] -> None
    | h::t ->
            if n-1=0 
                then Some h
                else fgetnth (n-1) t;;

let ()=
    let ()=fprint ( fgetnth 3 ["aa";"bb";"cc";"dd"]) in 
    let ()=fprint ( fgetnth 3  []) in 
    ();;
