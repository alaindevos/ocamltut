(* struct point ***************************************)
type point={x:int;y:int}
let addpoints (p1:point)(p2:point):point=
    let {x=x1;y=y1}=p1 in
    let {x=x2;y=y2}=p2 in 
    {x=x1+x2;y=y1+y2};;


