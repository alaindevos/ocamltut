(* curry & uncurry **************************************************)
type tuplexy=int *int
type recordxy={ x:int;y:int}
type intlist= int list
type intarray=int array 
let _=
	(* Two arguments , ie curried, can be partially applied*)
    (* A function given an integer x returns a function taking an integer y ************!!!*)
	let adda:int->(int->int) = fun x-> (fun y -> (x+y)) in
	let addb:int->(int->int) = fun x y -> x+y in
	let addc (x:int) (y:int):int = x+y in

	(* One tuple , ie uncurried *)
	let adde (xy:tuplexy):int = fst xy + snd xy in 
	(* One tuple pattern , ie uncurried *)
    let addd1 (xy:tuplexy):int = match xy with
        | (x,y) -> x+y in 
	let addd2 ((x:int), (y:int)):int = x+y in


	(*record*)
	let addf (xy:recordxy):int = match xy with
		| {x;y} -> x + y in 
		
	(*intlist*)
	let addg (alist:intlist):int=match alist with
		| x::y::[] -> x+y
		| _ -> 0 in

    let addh (ar:intarray):int = match ar with
        | [|x;y|] -> x+y 
        | _ -> 0 in 

    let helper = fun x y -> (x,y) in 
    let addi (str:string) = Scanf.sscanf str "%d %d" helper in 
    let (x,y)=addi "3 4" in
    let z=x+y in
    print_int z;

	let curry f x y = f (x,y) in
	let uncurry f (x,y) = f x y in 

	()
	
