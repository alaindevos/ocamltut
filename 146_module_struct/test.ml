module type COUNTERSIG =
    sig
        val c:int ref
        val add: unit -> unit 
        val print: unit -> unit
    end


module  Counter : COUNTERSIG =
    struct
        type intr=int ref 
        let c:intr =ref 0
        let add()=
            c :=  !c + 1
        let print()=print_int  !c
    end

let ()=
    Counter.print () ;
    Counter.add () ;
    Counter.add () ;
    Counter.print () ;
    ()

