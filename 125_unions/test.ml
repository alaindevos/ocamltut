type number  =
    | Zero
    | Integer of int
    | Real of float 

type atree=
    | Node of atree * int * atree
    | Leaf

type color=
    | Red
    | Black

type rbtree=
    | Nodex of color * rbtree * int * rbtree
    | Leafx

let _ =
    let zero=Zero in
    let i = Integer 1 in
    let f = Real 1.0 in
    let float_of_number x =
        match x with
        | Zero -> 0.0
        | Integer i ->  float_of_int i
        | Real f -> f in
    let add n1 n2 =
        match (n1,n2) with
        | (Zero,n2) -> n2
        | (n1,Zero) -> n1
        | (Integer i1,Integer i2) -> Integer (i1+i2)
        | (Integer i1,Real f2) -> Real  ((float_of_int i1) +.  f2)
        | (Real f1,Integer i2) -> Real  ( f1 +. (float_of_int i2))
        | (Real f1,Real f2)    -> Real  ( f1 +. f2) in
    (*  Unbalanced Binary tree *)
    let empty=Leaf in
    let insert x aset=Node(Leaf,x,aset) in
    let rec set_of_list lis=
        match lis with
        | [] -> empty
        | h::t -> insert h (set_of_list t) in
    let rec ismember x set=
        match set with
        | Leaf -> false
        | Node (left,y,right)-> 
                if (x=y) || (ismember x left) || (ismember x right) then true
                else false in
    (* Unbalanced ordered trees *)
    let rec insert_sorted x aset =
        match aset with
        | Leaf -> Node(Leaf,x,Leaf)
        | Node(left,y,right) ->
                if (x<y) then 
                    Node(insert x left,y,right)
                else if  (x>y) then
                    Node(left,y,insert x right)
                else
                    Node(left,y,right) in
    let set_of_list x=
        match x with
        | [] -> empty
        | h::t -> insert h (set_of_list t) in
    let rec ismember_sorted x s =
        match s with
        | Leaf -> false
        | Node(left,y,right) ->
                if ((x=y)  
                    || (x<y && ismember_sorted x left)
                    || (x>y && ismember_sorted y right))
                then
                    true
                else
                    false in
    (*Balanced red-black trees*)
    let rec memrb x s=
        match s with
        | Leafx -> false
        | Nodex  (_,left,y,right)  ->
                if  ((x=y)
                    ||((x<y) &&  (memrb  x left))
                    ||((x>y) &&  (memrb  x right)))
                then
                    true
                else
                    false in
    ()


