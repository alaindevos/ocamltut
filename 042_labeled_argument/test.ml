(* Labeled argument ************************************************)
type sumtype = x:int->y:int->int
let sum:sumtype = fun ~x ~y -> x+y;;
print_int (sum ~y:2 ~x:3);;

let sum2 ~(x:int) ~(y:int) :int =x+y;;

let myexp ~arg:x ~exp:y = x**y;;
let f=myexp ~arg:2.0 ~exp:3.0;;
print_string (string_of_float f);;

let myplus ~x:(xx:int) ~y:(yy:int) : int = xx+yy  ;;
let mysum= myplus ~x:8 ~y:6 ;;
print_int mysum ;;

