(* Tuple: unnamed *, use comma***************************************)
let l1=(10,10,"am");;
type mytime=int*int*string
let l2:mytime=(10,10,"am");;
type apair=int*int;;
let l3:apair=(1,2);;
print_int (fst(l3));;
print_int (snd(l3));;
type v=float*float*float;;
let printme ppf (x,y,z) =
  Printf.fprintf ppf "(%f,%f,%f)" x y z in
let myt=(1.,2.,3.:v) in
Printf.printf "%a" printme myt;;
