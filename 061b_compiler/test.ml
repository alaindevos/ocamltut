(* Compiler *****************************************)
(* ocamlopt:Nativecode compiler *)
(* Creates .o file *)
(* Run LINKED NATIVECODE *)
(* ocamlc:Bytecode compiler *)
(* Creates .cmi : "compiled" module interface *)
(* Creates .cmo : Compiled module object code in BYTECODE *)
(* ocamlopt test.ml -verbose -cc="gcc-10" -o test *)
(* ocamlc test.ml -verbose -cc=llc-11 -o test *)
(* Run LINKED BYTECODE *)
(* ocamlrun ./test *)

let _= print_string "Hello world"
