(* mutable int record ****************************************)
type anint = {mutable i:int}
let ()=
    let a:anint={i=3} in 
    let increment (x:anint)=
        x.i <- x.i + 1 ;
        () in
    increment a ;
    (* prints 4*)
    print_int a.i;;
