(* Class point ******************************************)
type  tpoint={mutable x:int ; mutable y:int}
class cpoint=object
    val p:tpoint={x=0;y=0}
    method set ~x ~y =
        p.x<-x;
        p.y<-y;
        ()
    method getx = p.x
    method gety = p.y
    method getxy = (p.x,p.y)
end;;
let p:cpoint=new cpoint in
p#set ~y:3 ~x:5;
let x=p#getx in
print_int x;
let (x,y)=p#getxy in
print_int y;;
