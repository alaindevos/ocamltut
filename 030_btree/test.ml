(** int binary tree ********************************************)
type bt =
    | Leaf
    | Node of int * bt * bt;;
let mytree = Node (5 , Leaf , Leaf);;
let isempty t =
    match t with
    | Leaf -> true
    | Node _ -> false;;
Printf.printf "%b" (isempty mytree);;
