(* Tail recursive fibonacci **********************)
let () =
  let fib (n : int) : int =
    let rec tail (n : int) (a : int) (b : int) : int =
      if n = 0 then a else tail (n - 1) b (a + b)
    in
    tail n 0 1
  in
  print_int (fib 10)
