class myintset = 
    object(this)
        val mutable p  =  []
        method contains x=
            let rec contains_inner x1 a1 =
                match a1 with
                | [] -> false
                | h::t -> 
                        if h = x1 
                        then true 
                            else contains_inner x1 t in
            contains_inner x p
        method add v =
            if this#contains v then ()
            else p<-v::p
        method  print = 
            let rec print_inner a = match a with
            | e::l -> 
                    print_int e ; 
                    print_string "\n" ; 
                    print_inner l
            |  [] -> () in
            print_inner p
    end;;
let _ =
    let s = new myintset in
    s#add 1;
    s#add 2;
    s#add 3;
    s#add 3;
    s#add 2;
    s#add 1;
    s#print;
    ()

    

                  



