type card = 
    | Card of regular
    | Joker
and regular = { suit : card_suit ; name : card_name }
and card_suit =
    | Heart
    | Club
    | Spade
    | Diamond
and card_name =
    | Ace
    | King
    | Queen
    | Jack
    | Simple of int
let _ = 
    let cj = Card { name = Jack ; suit = Club} in
()
