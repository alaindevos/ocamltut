	.file ""
	.section .rodata.cst16,"aM",@progbits,16
	.align	16
caml_negf_mask:
	.quad	0x8000000000000000
	.quad	0
	.align	16
caml_absf_mask:
	.quad	0x7fffffffffffffff
	.quad	-1
	.data
	.globl	camlHello__data_begin
camlHello__data_begin:
	.text
	.globl	camlHello__code_begin
camlHello__code_begin:
	.data
	.align	8
	.data
	.align	8
	.quad	768
	.globl	camlHello
camlHello:
	.data
	.align	8
	.globl	camlHello__gc_roots
camlHello__gc_roots:
	.quad	camlHello
	.quad	0
	.data
	.align	8
	.quad	2044
camlHello__1:
	.ascii	"Hallo"
	.space	2
	.byte	2
	.text
	.align	16
	.globl	camlHello__entry
camlHello__entry:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
.L101:
	movq	camlHello__1@GOTPCREL(%rip), %rbx
	movq	camlStdlib@GOTPCREL(%rip), %rax
	movq	304(%rax), %rax
	call	camlStdlib__output_string_245@PLT
.L100:
	movl	$1, %eax
	addq	$8, %rsp
	.cfi_adjust_cfa_offset -8
	ret
	.cfi_adjust_cfa_offset 8
	.cfi_adjust_cfa_offset -8
	.cfi_endproc
	.data
	.align	8
	.text
	.globl	camlHello__code_end
camlHello__code_end:
	.data
				/* relocation table start */
	.align	8
				/* relocation table end */
	.data
	.quad	0
	.globl	camlHello__data_end
camlHello__data_end:
	.quad	0
	.align	8
	.globl	camlHello__frametable
camlHello__frametable:
	.quad	1
	.quad	.L100
	.word	17
	.word	0
	.align	4
	.long	(.L102 - .) + 0
	.align	8
	.align	4
.L102:
	.long	(.L104 - .) + -1409286144
	.long	1966416
.L103:
	.ascii	"stdlib.ml\0"
	.align	4
.L104:
	.long	(.L103 - .) + 0
	.ascii	"Stdlib.print_string\0"
	.align	8
