(* Natural numbers **************************************************)
type nat = Zero | Succ of nat
let _=
    let  zero=Zero in
    let   one=Succ Zero in
    let   two=Succ one in
    let three=Succ two in

    let iszero n =match n with
        | Zero -> true
        | Succ _ -> false in 

    let pred n = match n with
        | Zero -> failwith "Undefined"
        | Succ m -> m in 

    let rec add x y = match x with
        | Zero -> y
        | Succ pred_x -> add pred_x (Succ y) in

    let rec int2nat i = match i with
        | i when (i = 0)-> Zero
        | i when (i > 0) -> Succ (int2nat (i -1))
        | i when (i < 0) -> failwith "Undefined" in 

    let rec nat2int n = match n with
        | Zero -> 0
        | Succ pred_n -> 1 + ( nat2int pred_n) in

    let rec even n = match n with
        | Zero -> true
        | Succ pred_n -> odd pred_n and

    odd n = match n with
        | Zero -> false
        | Succ pred_n -> even pred_n in

    ()
