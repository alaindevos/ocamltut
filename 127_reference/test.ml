let _ =  
    let factorial i =
        let j= ref 1 in
        for k = 2 to i 
        do
            j:=!j*k
        done;
        !j  in
    print_int (factorial 5);
    ()

