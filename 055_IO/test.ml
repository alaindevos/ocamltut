(* IO **********************************************)
print_int 100;;
print_string "abc";;
print_newline;;
let printstringint si=match si with
    | (s,i) -> print_newline ();
               print_string s;
               print_newline ();
               print_int i;;
printstringint ("abc",123);;
let rec printdic1 d=match d with
    | h::t -> printstringint h;
              printdic1 t
    | [] -> ();;
let adic=[("abc",123);("def",456)];;
printdic1 adic;; 
let rec myiter d f=match d with
    | h::t -> f h;
              myiter t f
    | [] -> ();;
let printdic2 d=myiter d printstringint;;
printdic2 adic;; 
let writestringint ch si=match si with
    | (s,i) -> output_string ch s;
               output_char ch '\n';
               output_string ch (string_of_int i);
               output_char ch '\n';;
let writedic ch d=myiter d (writestringint ch);;
let writedictofile fn d=
    let ch=open_out fn in
    writedic ch d;
    close_out ch;;
writedictofile "test.txt" adic;;
let readentry ch=
    let s=input_line ch in
    let i=(int_of_string (input_line ch)) in
    (s,i);;
let rec readdic ch=
    try
        let e=readentry ch in
        e :: (readdic ch)
    with
        End_of_file -> [];;
let readdictoffile fn=
    let ch=open_in fn in
        let d=readdic ch in
            close_in ch;
            d;;
let r=readdictoffile "test.txt";;
