class ['a] stack = object
    val mutable p : 'a list = []
    method push v =
        p <- v::p
    method pop =
        match p with
        | h::t -> p <- t;
                  h
        | [] -> failwith "Empty"
end
let _ =
    let s = new stack in
    s#push 2;
    s#push 3;
    print_int s#pop;
    ()


                  
