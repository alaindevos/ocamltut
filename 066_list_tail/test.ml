(* Get last of a string-list ***********************************)
type somestring= None | Some of string
type stringlist=string list 

let fgetstring (x:somestring):string = match x with
    | None -> "None"
    | Some x -> x;;

let fprintsomestring (x:somestring)=
    let laststring:string=fgetstring x in 
    let ()=print_string laststring in 
    ();;

let rec fgetlast (lst:stringlist):somestring =  match lst with 
    | [] -> None 
    | [x] -> Some x
    | _::t -> fgetlast t;;

let ()=
    let ()=fprintsomestring ( fgetlast ["aa";"bb";"cc";"dd"]) in 
    let ()=fprintsomestring ( fgetlast []) in 
    ();;
