(* object counter *****************************************)
let ob (init:int)=object
    val mutable i:int=init
    method inc () = i<-i+1
    method geti ():int =i
end

let _=
    let x=ob 2 in
    x#inc ();
    print_int (x#geti ());
    ()
