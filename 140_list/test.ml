let ()=
    let a:(int list)=[]  in
    let b:(int list)=1::[] in
    let c:(int list)=2::1::[] in
    let d:(int list)=2::(1::[]) in
    let is_empty ( lst : int list ) : bool =
        match lst  with
            | [] -> true
            | hd::tl -> false in
    ()

