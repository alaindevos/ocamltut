let ()=
    let f1 (x:float):float =
        if (x < 0.0)
        then
            raise( Failure "Invalid argument")
        else
            Float.sqrt(x) in

    let f2 (y:float):float =
        try
            let z=f1 y in
            z 
        with
            | Failure s -> print_string s;
                           0.0 in

    let a = -1.0 in
    let b = f2 a in
    print_float b;
    ()


