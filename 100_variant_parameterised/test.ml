(* Type list of alpha i.e. parameter *)
(* alpha is the parameter and comes BEFORE type-function alist*)
(* alpha is "pipelined" |> towards alist ; alist operates on type alpha *)
(* | is something else from "shell script " and means OR *)
(* 'a: head ; ('a alist):tail *)
type 'a alist = Nil | Cons of 'a * 'a alist

let (_ : unit) =
  let rec mylength (lst : 'a alist) : int =
    match lst with Nil -> 0 | Cons (h, t) -> 1 + mylength t
  in

  let rec myappend (lst1:('a alist)) (lst2:('a alist)):('a alist) = match lst1 with
    | Nil -> lst2 
    | Cons (h,t) -> Cons (h,myappend t lst2) in 

  (* a  list of int containing  1 and 2 *)
  let alist : int alist = Cons (1, Cons (2, Nil)) in
  let two : int alist = myappend alist alist in 
  let alen : int = mylength two in
  print_int alen

