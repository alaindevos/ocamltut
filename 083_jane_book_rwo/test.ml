open Core;;

Printf.printf "%d" 5

let sum (x : int) (y : int) : int = x + y;;

Printf.printf "%d" (sum 3 5)

let atuple = (3, "three")
let x, y = atuple
let alist = [ 1; 3; 3 ]
let myhead al = match al with [] -> [] | head :: tail -> head

type apoint = { x : float; y : float }

let ap : apoint = { x = 3.0; y = 4.0 }
let anarray = [| 1; 2; 3; 4 |];;

(* mutable *)
anarray.(0) <- 7;;
Printf.printf "%d" anarray.(0)

type ampoint = { mutable x : float; mutable y : float }

(* mutable *)
let amp : ampoint = { x = 3.0; y = 4.0 };;

amp.x <- 5.0

type arefx = { mutable contents : int }

let myref x = { contents = x }
let getcontents r = r.contents
let setcontents r x = r.contents <- x
let rx : arefx = { contents = 2 };;

rx.contents <- 3;;
setcontents rx (getcontents rx + 1)

let x = ref 0;;

x := !x + 1;;
Printf.printf "%d" !x

let sum = ref 0;;

List.iter alist ~f:(fun x -> sum := !sum + x);;

(* labeled arguments *)
for x = 0 to 5 do
  Printf.printf "%d" x
done

let myfun x = x + 1;;

Printf.printf "%d" (myfun 5)

let substract x y = x - y
let ( |> ) x f = f x;;

Printf.printf "%d" (-1 |> abs)

let myabs ~aval:x = abs x;;

Printf.printf "%d" (myabs ~aval:(-10))

let alist = [ 1; 2; 3 ];;

Printf.printf "%s" (String.concat [ "abc"; "def" ]);;
Printf.printf "%s" ("abc" ^ "def");;

let assoc = [ ("one", 1); ("two", 2) ] in
let mytoint x = match x with Some c -> c | None -> 0 in
let comparestrings x y =
  let z = String.compare x y in
  if z = 0 then true else false
in
let v = List.Assoc.find assoc ~equal:(fun x y -> comparestrings x y) "two" in
Printf.printf "%d" (mytoint v)
