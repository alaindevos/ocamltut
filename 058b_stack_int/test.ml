(* Class int stack ****************************************)
type intlist = int list
type someint = None | Some of int

class cintstack =
  object (self)
    val mutable s : intlist = [ 0; 1; 2; 3 ]
    method pop : someint = match s with [] -> None | h :: t -> Some h
    method push h = s <- h :: s

    method pop2 : int =
      let apop : someint = self#pop in
      match apop with None -> 0 | Some x -> x
  end

let () =
    let mystack = new cintstack in
    let () = mystack#push 4 in
    let () = mystack#push 5 in
    let so2 : int = mystack#pop2 in
    let () = print_int so2 in 
    ()
