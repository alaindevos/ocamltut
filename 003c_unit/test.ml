(* unit - let ******************************************************)
let (_:unit)=
    let myfunction :unit->unit = fun () -> print_string "Hallo" in
    myfunction (); 
    let myfunction2 (_: unit): unit = print_string "Hallo2" in 
    myfunction2 ()


