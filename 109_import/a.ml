module A : sig
        type nil=NIL 
        val idem:nil->nil 
        val f1:unit->unit
    end=struct
        type nil = NIL 
        let idem = fun x -> x
        let f1 = fun() ->  print_endline "\n A \n"
    end 

