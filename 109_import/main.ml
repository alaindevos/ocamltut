open A
open B

let main () =
    let () = print_endline "Start main" in 
    let _:A.nil = A.NIL in
    let _:A.nil = A.idem(A.NIL) in 
    let _:A.nil = A.idem(NIL) in   (* shorthand *) 
    let ()= A.f1 () in 
    let ()= B.f2 in  (* Does nothing as already been evaluated before start main *)  
    let ()= print_endline "Stop main" in 
    () 

let ()= main ()
