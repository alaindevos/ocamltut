(* Variant type **********************************************)
type colour=Red|Green|Blue;;
let col=Blue;;
let cols=[Red;Green;Blue];;
let components c=match c with
    | Red -> (255,0,0)
    | Green -> (0,255,0)
    | Blue -> (0,0,255);;
let myfirst t=match t with
    | (x,y,z) -> x;;
Printf.printf "%d\n" (myfirst (components Red));;
