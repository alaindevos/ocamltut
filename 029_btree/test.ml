(* int binary tree ***************************************************)
type bt = 
    | Empty
    | Fork of bt * int * bt ;;
let emptyt:bt = Empty ;;
let singeltont:bt = Fork(emptyt,5,emptyt);;

let rec member n  t =  match t with 
    | Empty -> false
    | Fork(l,m,r) ->
        if n < m then member n l
        else if n > m then member n r
        else true ;;
