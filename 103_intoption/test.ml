
type intoption= None | Some of int
let i0:intoption=None;;
let i1:intoption=Some 1;;
let i2:intoption=Some 2;;

let getval1 (x:intoption):int = match x with
    | None -> failwith "Error"
    | Some x -> x 


let getval2 (adefault:int) (x:intoption):int = match x with
    | None -> adefault 
    | Some x -> x 

let mydiv (x:int) (y:int):intoption= match y with
    | 0 -> None 
    | _ -> Some (x/y);; 

let intmax (x:int)(y:int):int=max x y;;

let rec listmax (lst: int list):intoption=match lst with
    | [] -> None
    | h::t -> 
            begin
                match listmax t with
                | None -> Some h
                | Some x -> Some (intmax h x)
            end;;
