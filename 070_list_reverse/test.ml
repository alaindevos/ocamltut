(* Reverse a list *****************************************)
type somestring= None | Some of string
type stringlist=string list 

let rec finverse (lst:stringlist):stringlist =match lst with
    | [] ->  []
    | h::t -> (finverse t) @ [h];;

let rec fprint_stringlist (lst:stringlist)=
    match lst with
    | [] -> ()
    | h::t -> let()=print_string h in 
              fprint_stringlist t;;

let ()=
    let ()=fprint_stringlist ( finverse ["aa";"bb";"cc";"dd"]) in 
    let ()=fprint_stringlist ( finverse  []) in 
    ();;

