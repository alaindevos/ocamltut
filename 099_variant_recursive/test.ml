(* Type list of integers *)
(**      int:head | intlist:tail    **)
type intlist = Nil | Cons of int * intlist

let (_ : unit) =
  let rec mylength (lst : intlist) : int =
    match lst with Nil -> 0 | Cons (h, t) -> 1 + mylength t
  in

  let rec myappend (lst1:intlist) (lst2:intlist):intlist = match lst1 with
    | Nil -> lst2 
    | Cons (h,t) -> Cons (h,myappend t lst2) in 

  (* List containing 1 and 2*)
  let alist : intlist =  Cons(1,Cons(2,Nil)) in 
  let two:intlist= myappend alist alist in 
  let alen : int = mylength two in
  print_int alen
