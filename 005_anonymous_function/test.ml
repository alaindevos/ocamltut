(* anonymous function ************************************************************)
print_int   ( (fun x -> x + 1) 15 );;
print_float ( (fun x y -> x +. y) 15.0 3.0);;
print_int ((fun x-> x+1) 6);;
print_int ((fun x y -> (x+y)/2) 1 2 );;
(* named *)
let f=(fun x -> x +1);;
print_int (f 5);;
(* without fun keyword *)
let f x=x+1;;
let avg x y = (x+y)/ 2;;
(* equivalent*)
(fun x -> x+1) 2;;
let x=2 in x+1;;
(*in*)
let f x y=x-y in
    f 3 2;;
let f = fun x y -> x-y in
    f 2 3;;
    
