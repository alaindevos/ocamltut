open Printf

let enter_callback entry =
    let () = printf "Entry contents: %s\n" entry#text in
    flush stdout

let entry_toggle_editable button entry = entry#set_editable button#active
let entry_toggle_visibility button entry = entry#set_visibility button#active

let main () =
    let _  = GMain.init () in
    let window = GWindow.window ~title:"GTK Entry" ~width:200 ~height:100 () in
    let _ = window#connect#destroy ~callback:GMain.quit in

    let vbox = GPack.vbox ~packing:window#add () in

    let entry = GEdit.entry ~max_length:50 ~packing:vbox#add () in
    let _ = entry#connect#activate ~callback:(fun () -> enter_callback entry) in
    let () = entry#set_text "Hello" in
    (* Appending text now requires getting the underlying buffer, and
     * entry#buffer is not exposed in the bindings yet *)
    (*entry#append_text " world";*)
    let () = entry#select_region ~start:0 ~stop:entry#text_length in

    let hbox = GPack.hbox ~packing:vbox#add () in

    let check =
        GButton.check_button ~label:"Editable" ~active:true ~packing:hbox#add ()
    in
    let _ =
        check#connect#toggled ~callback:(fun () ->
            entry_toggle_editable check entry)
    in

    let check =
        GButton.check_button ~label:"Visible" ~active:true ~packing:hbox#add ()
    in
    let _ =
        check#connect#toggled ~callback:(fun () ->
            entry_toggle_visibility check entry)
    in

    let button = GButton.button ~label:"Close" ~packing:vbox#add () in
    let _ = button#connect#clicked ~callback:window#destroy in
    let () = button#grab_default () in

    let () = window#show () in

    let () = GMain.main () in
    ()

let () = main ()
