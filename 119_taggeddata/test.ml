type myif =
    | Integer of int
    | Float of float

let _ =
    let mysum = fun ( x : myif ) ( y : myif ) : myif  ->
        match (x,y) with
        | (Integer i, Integer j) -> 
                let s=i+j in
                Integer s
        | (Float f , Float g)-> 
                let s = f +. g in
                Float s 
        | (_,_) -> Float 0.0 in
    let printme = fun ( x : myif ) : unit ->
        match x with
        | Integer i -> print_int i
        | Float f -> print_float f in
    printme (mysum (Integer 2) (Integer 3)) ;
    printme (mysum (Float 2.0) (Float 3.0)) ;
    ()

