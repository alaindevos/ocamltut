(* module struct *************************************)
module ModuleTest = struct
    let a=2
end

let ()= 
    let ()=print_int 1 in
    let ()=print_int ModuleTest.a in 
    let ()=ModuleTest.(print_int a) in
    ()
