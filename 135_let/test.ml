type operand=
    | MyPlus
    | MyMin

type expr =
    | Leaf of int 
    | Node of node 
    | Cnd of cnd
    | Mylet of mylet
    | Myvar of string
and
 node={myop : operand ; expr1 : expr ; expr2 : expr} 
and
 cnd={mytest : expr ;  mythen : expr ; myelse : expr }
and
 mylet={ myvar : string ; myvalue : expr ; mybody : expr} 

let _ =

    let ht=Hashtbl.create 0 in 

    let rec myeval  = fun x ->
        match x with
        | Leaf l -> l
        | Node n -> if (n.myop = MyPlus) 
        then

            (myeval n.expr1) +(myeval n.expr2)
       
        else
            if (n.myop = MyMin )
            then
            (myeval n.expr1) - (myeval n.expr2)
        else
            -1000
       | Cnd cnd ->
               if (myeval (cnd.mytest)) != 0 then
                   myeval cnd.mythen
               else
                   myeval cnd.myelse
       | Myvar myvar -> Hashtbl.find ht myvar  
       | Mylet mylet ->Hashtbl.add ht mylet.myvar (1 + myeval mylet.myvalue);
                       myeval mylet.mybody in

    let  myexpr1 = Leaf 1 in
    let  myexpr2 = Node { myop = MyPlus ; expr1 = Leaf 2 ; expr2 = Leaf 3} in
    let  myexpr3 = Cnd  { mytest= Leaf 1 ; mythen = Leaf 2 ; myelse = Leaf 3 } in
    let  myexpr4 = Mylet { myvar="x" ; myvalue=Leaf 321 ; mybody = Myvar "x" } in
    let  x = myeval myexpr1 in
    let  y = myeval myexpr2 in
    let  z = myeval myexpr3 in
    let  a = myeval myexpr4 in 
    (Printf.printf "%d" x);
    (Printf.printf "%d" y);
    (Printf.printf "%d" z);
    (Printf.printf "%d" a);
    ()

