(* Book ocaml from the very beginning *********************)
exception MyFailure of string;;
type mycolor = Red | Green | Blue;;
type someint = 
    | None 
    | Someint of int;;
type mypoint = {x:int;y:int};;
let printsomeint i=match i with
    | None -> ()
    | Someint ii -> Printf.printf "Koekoe:%i" ii in
let mytest()=
    try
        Printf.printf "AAA\n" ;
        raise (MyFailure "This went wrong")
    with
        MyFailure s -> 
            Printf.printf "MYFAILURE: %s\n" s in
let rec myprintdic d=match d with
        | []       -> Printf.printf "\n"
        | (k,v)::t -> (Printf.printf "%d %s\n" k v) ;
                      (myprintdic t) in
let rec mywrite ch d=match d with
    | [] -> ()
    | (k,v)::t -> output_string ch (string_of_int k);
                  output_string ch (string_of_int v);
                  mywrite ch t in
let myswap x y =
    let t= !x in
    x:= !y;
    y:= t;
    () in
let mydouble ~x = 2 * x in
let main()=
    mytest();
    Printf.printf "Hallo\n";
    Printf.printf "%d\n" (if 100 > 99 then 0 else 1);
    let a=500 in
    let b=a*a in
    let cube x=x*x*x in
    Printf.printf "Cube:%d\n" (cube b);
    let neg1 x=if x<0 then true else false in
    Printf.printf "%B\n" (neg1 (-30));
    let neg2 x= x<0 in
    Printf.printf "%B\n" (neg2 (-30));
    let isvowel c= c='a' || c='i' || c='e' || c='o' || c='u' in
    Printf.printf "%B\n" (isvowel 'a');
    let addtoten a b= a + b = 10 in
    Printf.printf "%B\n" (addtoten  3 7);
    let rec factorial1 x=
        if x=1 then 1
        else x * factorial1 (x-1) in
    Printf.printf "%d\n" (factorial1 10);
    let rec factorial2 x=
        match x with 
          |  1  -> 1
          | _  -> x * factorial2 (x-1) in
    Printf.printf "%d\n" (factorial2 10);
    let al1=[1;2;3] in 
    let al2=0:: al1 in 
    let al3= [-2 ; -1] @ al2 in
    List.iter (Printf.printf "%d ") al3 ;
    let isnil al=match al with
        |  [] -> true
        |  _  -> false in
    Printf.printf "%B\n" (isnil []) ;
    let rec mylen al=match al with
        |  []   -> 0
        |  h::t -> 1+(mylen t) in
    Printf.printf "%d\n" (mylen [1;2;3]) ;
    let rec mylen2 al n =match al with
        | []   -> n
        | h::t -> mylen2 t (n+1) in
    let rec mylen3 al=mylen2 al 0 in
    Printf.printf "%d\n" (mylen3 [1;2;3]) ;
    let rec mysum al=match al with
        |  []   -> 0
        |  h::t -> h+(mysum t) in
    Printf.printf "%d\n" (mysum [1;2;3]) ;
    let rec oddelements al=match al with
        | h1::_::t -> h1::(oddelements t)
        | h::_    ->  [h] 
        | []      -> [] in
    List.iter (Printf.printf "%d ") (oddelements [1;2;3;4;5;6]) ;
    let rec myappend x y=match x with
        | []   -> y
        | h::t -> h::(myappend t y) in
    List.iter (Printf.printf "%d ") (myappend [1;2;3] [4;5;6]) ;
    let rec myreverse x=match x with
        | []    -> []
        | h::t  -> (myreverse t) @ [h] in
    List.iter (Printf.printf "%d ") (myreverse [1;2;3]) ;
    let rec mytake al n=match n with
        | 0 -> []
        | _ -> match al with
                | h::t -> h::(mytake t (n-1)) 
                | _    -> [] in
    List.iter (Printf.printf "%d ") (mytake [1;2;3;4;5;6] 3) ;
    let rec mydrop al n=match n with
        | 0 -> al
        | _ -> match al with
                | h::t ->  mydrop t (n-1) 
                | []   ->  [] in
    List.iter (Printf.printf "%d ") (mydrop [1;2;3;4;5;6] 3) ;
    let rec mymap afun al =match al with
        | []   ->  []
        | h::t ->  (afun h)::(mymap afun t) in
    List.iter (Printf.printf "%d ") (mymap (fun n -> 2*n) [1;2;3;4;5]) ;
    let kv1=( 1 , "Alain") in
    let myfirst kv=match kv with
        (x,_) -> x in
    let mylast kv=match kv with
        (_,y) -> y in
    Printf.printf "%d" (myfirst kv1);
    Printf.printf "%s" (mylast kv1);
    let mydict=[ (1,"Alain") ; (2,"Eddy")] in
    let rec mylookup d x=match d with
        | [] -> raise Not_found
        | (k,v)::t ->
            if k=x then v
            else (mylookup t x) in
    Printf.printf "%s\n" (mylookup mydict 2) ;
    let rec myaddkv d k v=match d with
        | [] -> [(k,v)]
        | (kh,vh)::t ->
            if k=kh 
                then (k,v)::t
            else
                (kh,vh)::(myaddkv t k v) in
    myprintdic ( [ (1,"Alain") ; (2,"Eddy")]) ;
    myprintdic (myaddkv [ (1,"Alain") ; (2,"Eddy")] 2 "Jos");
    let colortovalue c=match c with
        | Red   -> 0
        | Green -> 1
        | Blue  -> 2 in
    Printf.printf "%d\n" (colortovalue Green);
    let myfilename="out.txt" in
    let outchannel=open_out myfilename in
    mywrite outchannel [(1,2);(3,4)];
    close_out outchannel;
    let si=ref (Someint 3) in
    si:=(Someint 4);
    printsomeint !si;
    let ref3=ref 3 in
    let ref4=ref 4 in
    myswap ref3 ref4;
    Printf.printf "%d\n" (List.length [1;2;3]);
    Printf.printf "%d\n" (List.nth [1;2;3] 2);
    let apoint:mypoint={x=3;y=4;} in
    print_int apoint.x;
    Printf.printf "%d\n" (mydouble ~x:5);
    () 
    in
main ();;
