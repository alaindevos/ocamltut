type 'a alist =
  | Cons
  | Nil of 'a * 'a alist

let (_ : unit) =
  let rec mylength (lst : 'a alist) : int =
    match lst with
    | Cons -> 0
    | Nil (h, t) -> 1 + mylength t
  in
  let rec ( @ ) (lst1 : 'a alist) (lst2 : 'a alist) : 'a alist =
    match lst1 with
    | Cons -> lst2
    | Nil (h, t) -> Nil (h, ( @ ) t lst2)
  in
  let alist : int alist = Nil (1, Nil (2, Cons)) in
  let two : int alist = ( @ ) alist alist in
  let alen : int = mylength two in
  print_int alen
;;

