(* Lists are singly-linked & immutable, *****************************************)
let _=
    let l1:'a list=[] in
    let l2=[1]::l1 in
    let l3=[1;2]::l2 in
    let l4=[1;2;3;4]::l3 in
    print_int (List.hd (List.hd l4));
    let l5: int list list=[[1;2];[3;4]] in
    print_int (List.hd (List.hd l5)) in 
    let l6=1::[2;3] in
    print_int (List.hd l6);
    let l7=1::2::3::[4] in
    print_int (List.hd l7);
    let l8: int list=1::(2::(3::[])) in
    print_int (List.hd l8); 
    let a=List.length [1;2;3] in
    print_int a;
    let b=List.map String.length ["aaa";"bb";"c"] in
    print_int (List.hd b);
    let l=List.length [1;2;3;4;5] in
    print_int l;
    let ()=print_int (List.nth [1;2;3;4;5] 3) in
    print_int (List.hd [1;2;3])

