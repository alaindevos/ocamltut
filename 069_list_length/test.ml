(* Length of a list *************************************)
type somestring= None | Some of string
type stringlist=string list 

let rec fgetlength (lst:stringlist):int =match lst with
    | [] ->  0
    | h::t -> 1 + fgetlength t 

let ()=
    let ()=print_int ( fgetlength ["aa";"bb";"cc";"dd"]) in 
    let ()=print_int ( fgetlength  []) in 
    ();;
