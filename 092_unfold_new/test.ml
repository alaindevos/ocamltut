let _ =
  let helper z = match z with a, b -> Some (a, (b, a + b)) in
  let u = Seq.unfold helper (0, 1) in
  let t = Seq.take  7 u in
  let _ = Seq.iter print_int t in
  ()

