(* Operator function ************************************************************)
let _=
    let ( ^^ ) x y = x+y in
    let asum = 5 ^^ 3 in
    print_int asum
