type itree = 
    | Empty
    | Node of (itree*int*itree)
let ()=
    let  it:itree=Node(Node(Empty,1,Empty),2,Node(Empty,3,Empty))  in
    let  rec itsum(t:itree):int=match t with
        | Empty -> 0
        | Node (l,x,r)-> (itsum l)+x+(itsum r) in
    let z=itsum it in
    print_int z;
    ()
