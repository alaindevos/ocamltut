let ()=
    let t:(int*string)=(5,"Alain") in
    match t with
        | (x,y)  -> print_int x;
                    print_string y in
    let int_of_bool (c:bool):int=
        match c with
            | true -> 1
            | false -> 0 in
    print_int (  int_of_bool true );
    ()
