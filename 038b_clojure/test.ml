(* clojure mutable ref counter **************************)
let _=
    let counter = ref 1 in 
    let next : unit-> int =
        fun () -> 
            let ()= counter:=!counter+1 in 
            !counter in 
    let _:int= next () in 
    print_int (next ())
    
