(* List implementation **********************************************************)
type node={
    value:int;
    mutable next:node option;
    }

type mlist={
    mutable first:node option;
    }

let create_node v : node={
    next:node option=None;
    value=v;
    }

let singleton v:mlist={
    first:node option = Some (create_node v)
    }

let insert_first lst v=match lst.first with
    | None -> lst.first <-Some (create_node v);
    | Some was_first ->
         let new_first = create_node v in
         new_first.next<-Some was_first;
         lst.first<-Some new_first

let empty () : mlist ={
    first=None
    }

let n1:mlist=empty();;
insert_first n1 1;;
insert_first n1 2;;
let n2:mlist=singleton 3;;
insert_first n2 4;;
insert_first n2 5;;

let print_node (n:node)  =
    print_int n.value;;

let print_mlist (lst:mlist) =
    let no:node option ref= ref lst.first in
    while Option.is_some !no do 
        let n:node=Option.get !no in 
        let ()= print_node n in
        let ()= no:=n.next in 
        ()   
    done;;

print_mlist n1;;
print_mlist n2;;

