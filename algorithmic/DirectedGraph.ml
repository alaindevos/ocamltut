
module DirectedGraph
	:
sig
	type t
	val empty : t
	val add : int -> int -> t -> t
	val member : int -> int -> t -> bool
	val successors : int -> t -> int list
	val predecessors : int -> t -> int list
	val subgraph : t -> t -> bool
	val equal : t -> t -> bool
end
	=
struct

	type t =
		| Empty
		| Fork of t * int * int * t
		
	let empty =
		Empty
	
	(* add an edge (x,y) to the graph *)
	let rec add x y = function
		| Empty -> Fork(Empty,x,y,Empty)
		| Fork (l,u,v,r) as g ->
			if x < u then Fork (add y x l,u,v,r)
			else if x > u then Fork (l,u,v,add y x r)
			else if y = v then g
			else Fork (l,u,v,add y x r)
		(* else Fork (l,u,(min v y),add (max v y) x r)  proposed by gasche *)
		
	(* is the edge (x,y) in the graph ? *)
	let rec member x y = function 
		| Empty -> false
		| Fork (l,u,v,r) ->
			if x < u then member y x l
			else if x > u then member y x r
			else if y = v then true
			else member y x r

	(* deal with the non-discriminating levels *)
	let apply f w acc = function
		| Empty -> acc
		| Fork (l,u,v,r) ->
			f w (f w (if w = v then u::acc else acc) r) l

	(* deal with the discriminating levels *)
	let rec loop x acc = function    
		| Empty -> acc
		| Fork (l,u,v,r) ->
			if x < u then apply loop x acc l
			else if x > u then apply loop x acc r
			else v::apply loop x acc r
	(* successors   *)
	let successors x =
		loop x []
	(* predecessors *)
	let predecessors y =
		apply loop y []
		  			
	(* is ga a subgraph of gb ? *)						
	let subgraph ga gb =
		let rec sub1 = function
			| Empty -> true
			| Fork (l,u,v,r) -> member u v gb && sub2 l && sub2 r
		and sub2 = function
			| Empty -> true
			| Fork (l,u,v,r) -> member v u gb && sub1 l && sub1 r
		in sub1 ga
		
	(* graph equality *)
	let equal ga gb =
		subgraph ga gb && subgraph gb ga
  
end
