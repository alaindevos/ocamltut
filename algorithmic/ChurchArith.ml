
module ChurchArith
	:
sig
	type 'a t
	val zero : 'a t
	val one : 'a t
	val two : 'a t
	val three : 'a t
	val inc : 'a t -> 'a t
	val add : 'a t -> 'a t -> 'a t
	val mul : 'a t -> 'a t -> 'a t
	val power : 'a t -> ('a -> 'a) t -> 'a t
	val to_int : int t -> int
end
	=
struct
	type 'a t =
		('a -> 'a) -> ('a -> 'a)
	let zero f x =
		x
	let one f x =
		f x
	let two f x =
		f (f x)
	let three f x =
		f (f (f x))
	let inc n f x =
		f (n f x)
	let add n m f x =
		n f (m f x)
	let mul n m f x =
		n (m f) x
	let power n m f x =
		m n f x
	let to_int t =
		t succ 0
end

module CA = ChurchArith

CA.to_int (CA.power CA.three CA.two);;

module ChurchArith
	:
sig
	type t
	val zero : t
	val one : t
	val two : t
	val three : t
	val inc : t -> t
	val add : t -> t -> t
	val mul : t -> t -> t
	val power : t -> t -> t
	val to_int : t -> int
end
	=
struct
	type t =
		{c: 'a. ('a -> 'a) -> ('a -> 'a)}
	let zero =
		{c = fun f x -> x}
	let one =
		{c = fun f x -> f x}
	let two =
		{c = fun f x -> f (f x)}
	let three =
		{c = fun f x -> f (f (f x))}
	let inc n =
		{c = fun f x -> f (n.c f x)}
	let add n m =
		{c = fun f x -> n.c f (m.c f x)}
	let mul n m =
		{c = fun f x -> n.c (m.c f) x}
	let power n m = 
		{c = fun f x -> m.c n.c f x}
	let to_int t =
		t.c succ 0
end

module CA = ChurchArith

CA.add CA.three CA.two;;
