
module RandomAccessList
	:
sig
	type +'a t
	val empty : 'a t
	val is_empty : 'a t -> bool
	val singleton : 'a -> 'a t 
	val size : 'a t -> int
	val add : 'a -> 'a t -> 'a t
	val member : int -> 'a t -> 'a
	val remove : 'a t -> 'a t
	val replace : int -> 'a -> 'a t -> 'a t
	val meld : 'a t -> 'a t -> 'a t
end
	=
struct (*...*)

	type +'a t =
		| Empty
		| Zero of ('a * 'a) t
		| One of 'a * ('a * 'a) t
	
	let empty =
		Empty
			
	let is_empty l =
		l = Empty

	let singleton x =
		One(x,Empty)

	let rec size : 'a . 'a t -> int =
		function
		| Empty -> 0
		| Zero t -> 2 * size t
		| One (_,t) -> 1 + 2 * size t
	
	let rec add : 'a . 'a -> 'a t -> 'a t =
		fun x -> function
		| Empty -> One (x,Empty)
		| Zero t -> One(x,t)
		| One(y,t) -> Zero(add (x,y) t)
	
	let rec member : 'a . int -> 'a t -> 'a =
		fun i -> function
		| Empty -> invalid_arg "RandomAccessList.member"
		| One (x,_) when i = 0 -> x
		| One (_,t) -> member (i - 1) (Zero t)
		| Zero t ->
			let x,y = member (i / 2) t in
			if i mod 2 = 0 then x else y
	
	let rec remove : 'a . 'a t -> 'a * 'a t =
		function
		| Empty -> invalid_arg "RandomAccessList.remove"
		| One (x, Empty) -> x,Empty
		| One (x, t) -> x,Zero t
		| Zero t -> let (x,y),s = remove t in x,One(y,s)
		
	let remove t =
		snd (remove t)
	
	let replace i v =
		let rec go : 'a . ('a -> 'a) -> int -> 'a t -> 'a t =
			fun f n -> function
			| Empty -> invalid_arg "RandomAccessList.replace"
			| One(x,t) ->
				if n=0 then One(f x,t)
				else add x (go f (n - 1) (Zero t))
			| Zero t ->
				let g (x,y) = if n mod 2 = 0 then (f x, y) else (x, f y) in
				Zero (go g (n / 2) t)
		in go (fun x -> v) i

	(* like an append but does not preserve item rank *)
	let rec meld : 'a . 'a t -> 'a t -> 'a t =
		fun la lb -> match la,lb with
		| Empty 		 , 		  ta
		|			 ta , Empty			-> ta
		| Zero	 ta , Zero	  tb	-> Zero (meld ta tb)
		| Zero	 ta , One (x, tb)
		| One (x, ta), Zero	  tb	-> One (x, meld ta tb)
		| One (x, ta), One (y, tb) -> Zero (add (x, y) (meld ta tb))
  
end
