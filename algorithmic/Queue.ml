
(* FIFO queue *)
module Queue
:
sig
	type +'a t
	val empty : 'a t
	val member : 'a t -> 'a
	val add : 'a -> 'a t -> 'a t
	val remove : 'a t -> 'a t
end
=
struct
	type +'a t =
		{front:'a list;rear:'a list}
	let empty =
		{front=[];rear=[]}
	let member {front;_} =
		match front with
		| [] -> failwith "Queue.member"
		| h::_ -> h
	let queue front rear =
		match front with
		| [] -> {front=List.rev rear;rear=[]}
		| _ -> {front;rear}		
	let add n {front;rear} =
		queue front (n::rear)
	let remove {front;rear} =
		match front with
		| [] -> failwith "Queue.remove"
		| _::t -> queue t rear
end
