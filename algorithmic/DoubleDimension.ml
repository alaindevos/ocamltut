

module DoubleDimension
	:
sig
	type (+'a,+'b) t
	val empty : ('a,'b) t
	val add : 'a -> 'b -> ('a,'b) t -> ('a,'b) t
	val member : 'a -> 'b -> ('a,'b) t -> bool
	val first :  'b -> ('a,'b) t -> 'a list
	val second : 'a -> ('a,'b) t -> 'b list
	val for_all : ('a -> 'b -> bool) -> ('a,'b) t -> bool
	val subset : ('a,'b) t -> ('a,'b) t -> bool
	val equal :  ('a,'b) t -> ('a,'b) t -> bool
	val to_list : ('a,'b) t -> ('a * 'b) list
end
	=
struct (*...*)

	type (+'a,+'b) t =
		| Empty 
		| Fork of ('b,'a) t * 'a * 'b * ('b,'a) t

	let empty =
		Empty 
		
	let rec add : 'a 'b . 'a -> 'b -> ('a,'b) t -> ('a,'b) t =
		fun x y -> function 
		| Empty -> Fork(Empty,x,y,Empty)
		| Fork (l,u,v,r) as g ->
				if x < u then Fork (add y x l,u,v,r)
				else if x > u then Fork (l,u,v,add y x r)
				else if y = v then g
				else Fork (l,u,v,add y x r)
		
	let rec member : 'a 'b . 'a -> 'b -> ('a,'b) t -> bool =
		fun x y -> function 
		| Empty -> false
		| Fork (l,u,v,r) ->
				if x < u then member y x l
				else if x > u then member y x r
				else if y = v then true
				else member y x r

	let apply f w acc = function
		| Empty -> acc
		| Fork (l,u,v,r) ->
			f w (f w (if w = v then u::acc else acc) r) l
			
	let rec loop key acc = function
		| Empty -> []
		| Fork (l,u,v,r) ->
			if key < u then apply loop key acc l
			else if key > u then apply loop key acc r
			else v::apply loop key acc r
			
	let first y =
		apply loop y []
		
	let second x =
		loop x []
				
	let rec for_all :
		'a 'b .
		('a -> 'b -> bool) -> ('b -> 'a -> bool) -> ('a,'b) t -> bool
		=
		fun p q -> function 
		| Empty -> true 
		| Fork (l,u,v,r) -> for_all q p l && p u v && for_all q p r

	(* apply 2 function arguments in swapped order *)
	let flip f x y =
		f y x
		
	(* checks if all elements satisfy the predicate p *)
	let for_all p =
		for_all p (flip p)

	(* is ta a subset of tb ? *)						
	let subset ta tb =
		for_all (fun a b -> member a b tb) ta

	(* set equality *)
	let equal ta tb =
		subset ta tb && subset tb ta
	
	(* linearization *)	
	let rec to_list :
		'a 'b . 
		'c list -> ('a -> 'b -> 'c) -> ('b -> 'a -> 'c) -> ('a,'b) t -> 'c list
		= 
		fun acc fu fv -> function
		| Empty ->
			acc
		| Fork (l,u,v,r) -> 
			to_list (fu u v::to_list acc fv fu r) fv fu l
	
	let to_list t =
		to_list [] (fun u v -> u,v) (fun v u -> u,v) t
				
end
