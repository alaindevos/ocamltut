
(* ocamlopt.opt -o KnightTour -unsafe -w +A-ez KnightTour.ml *)

let board =
	Array.make_matrix 8 8 0

let bounded x y =
	if x < 0 || x > 7 then false
	else if y < 0 || y > 7 then false
	else true

let print_board () =
	Array.iter (fun a -> Array.iter
	(fun k -> if k < 10 then print_char '0'; print_int k; print_char ' ') a;
	print_newline ())
	board

let rec forward_chaining n x y =
	if bounded x y && board.(x).(y) = 0 then begin
		board.(x).(y) <- n;
		if n = 8 * 8 then
			(print_board (); print_newline ())
		else begin 
			forward_chaining (n + 1) (x + 1) (y + 2);
			forward_chaining (n + 1) (x + 2) (y + 1);
			forward_chaining (n + 1) (x + 2) (y - 1);
			forward_chaining (n + 1) (x + 1) (y - 2);
			forward_chaining (n + 1) (x - 1) (y - 2);
			forward_chaining (n + 1) (x - 2) (y - 1);
			forward_chaining (n + 1) (x - 2) (y + 1);
			forward_chaining (n + 1) (x - 1) (y + 2);
		end;
		board.(x).(y) <- 0;
	end

let () = forward_chaining 1 0 0
