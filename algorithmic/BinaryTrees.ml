

(* binary tree set *)

type t =
	| Empty
	| Fork of t * int * t

let empty =
	Empty	

let is_empty t =
   t = Empty

let singleton n =
	Fork(Empty,n,Empty)	

let rec strahler = function
	| Empty -> 0
	| Fork(l,n,r) ->
		let sl = strahler l and sr = strahler r in
		if sl = sr then sl+1 else max sl sr  
	
let rec add n t =
	match t with
	| Empty -> singleton n
	| Fork(l,m,r) ->
			if n < m then Fork(add n l,m,r) 
			else if n > m then Fork(l,m,add n r)
			else t

let rec member n = function
	| Empty -> false
	| Fork(l,m,r) ->
			if n < m then member n l 
			else if n > m then member n r
			else true

let rec interval low high = function
	| Empty -> Empty
	| Fork(l,n,r) ->
			if high < n then interval low high l
			else if low > n then interval low high r
			else Fork(interval low high l,n,interval low high r)

let rec cardinal = function
	| Empty -> 0
	| Fork(l,_,r) -> cardinal l + 1 + cardinal r
	
let rec cardinal acc = function
	| Empty -> acc
	| Fork(l,_,r) -> cardinal (cardinal acc r + 1) l

let cardinal =
	cardinal 0	  
			
let rec minimum acc = function
	| Empty -> acc
	| Fork(l,n,r) -> minimum n l

let rec remove_minimum la na ra =
	match la with
	| Empty -> ra
	| Fork(lb,nb,rb) -> Fork(remove_minimum lb nb rb,na,ra)
	
let concat ta tb =
	match ta,tb with
	| _,Empty -> ta
	| Empty,_ -> tb
	| _,Fork(lb,nb,rb) ->
			Fork(ta,minimum nb lb,remove_minimum lb nb rb)

let rec remove n = function
	| Empty -> Empty
	| Fork(l,m,r) ->
			if n < m then Fork(remove n l,m,r) 
			else if n > m then Fork(l,m,remove n r)
			else concat l r

let rec filter cond = function 
	| Empty -> Empty
	| Fork(l,n,r) ->
			if cond n then Fork(filter cond l,n,filter cond r)
			else concat (filter cond l) (filter cond r)	
			
let rec split n = function
	| Empty -> Empty,false,Empty
	| Fork(l,m,r) ->
			if n < m then 
				let la,present,ra = split n l in la,present,Fork(ra,m,r)
			else if n > m then
				let lb,present,rb = split n r in Fork(l,m,lb),present,rb
			else l,true,r	
			
let rec union ta tb =
	match ta,tb with
	| _,Empty -> ta
	| Empty,_ -> tb
	| Fork(la,na,ra),_ -> 
			let lb,_,rb = split na tb
			in Fork(union la lb,na,union ra rb)

let rec intersection ta tb =
	match ta,tb with
	| _,Empty | Empty,_ -> Empty
	| Fork(la,na,ra),_ -> 
			let lb,present,rb = split na tb in
			if present then Fork(intersection la lb,na,intersection ra rb)
			else concat (intersection la lb) (intersection ra rb)
   
let rec difference ta tb =
	match ta,tb with
	| _,Empty -> ta
	| Empty,_ -> Empty
	| Fork(la,na,ra),_ -> 
			let lb,present,rb = split na tb in
			if present then concat (difference la lb) (difference ra rb)
			else Fork(difference la lb,na,difference ra rb)

let rec disjoint ta tb =
	match ta,tb with
	| _,Empty | Empty,_ -> true
	| Fork(la,na,ra),_ -> 
			let lb,present,rb = split na tb in
			if present then false
			else disjoint la lb && disjoint ra rb
	
let rec subset ta tb =
	match ta,tb with
	| Empty,_ -> true
	| _,Empty -> false
	| Fork(la,na,ra),Fork(lb,nb,rb) -> 
			if na < nb then
				subset (Fork(la,na,Empty)) lb && subset ra tb
			else if na > nb then
				subset (Fork(Empty,na,ra)) rb && subset la tb
			else
				subset la lb && subset ra rb
	
let equal ta tb =
	subset ta tb && subset tb ta

let rec to_list = function
	| Empty -> []
	| Fork(l,n,r) -> to_list l @ [n] @ to_list r 

let rec to_list acc = function
	| Empty -> acc
	| Fork(l,n,r) -> to_list (n::to_list acc r) l
let to_list =
	to_list []

	
	
(* Braun stack *)

type t =
	| Empty
	| Fork of t * int * t

let empty =
	Empty	

let singleton n =
	Fork(Empty,n,Empty)	
	
let rec add n = function
	| Empty -> singleton n
	| Fork(l,m,r) -> Fork(add m r,n,l)
	
let rec member i = function 
	| Empty -> invalid_arg "BraunStack.member"
	| Fork(l,n,r) ->
		if i = 0 then n else
		if i land 1 = 1 then member (i / 2) l
		else member (i / 2 - 1) r
		
let rec concat ta tb =
	match ta with
	| Empty -> Empty
	| Fork(l,n,r) -> Fork(tb,n,concat l r)

let remove = function
	| Empty -> invalid_arg "BraunStack.remove"
	| Fork(l,_,r) -> concat l r

let rec replace i x = function 
	| Empty -> invalid_arg "BraunStack.replace"
	| Fork(l,y,r) ->
		if i = 0 then Fork(l,x,r) else
		if i land 1 = 1 then Fork(replace (i / 2) x l,y,r)
		else Fork(l,y,replace (i / 2 - 1) x r)
		
let rec diff n = function
	| Empty -> if n = 0 then 0 else assert false
	| Fork(l,_,r) ->
		if n = 0 then 1
		else if n mod 2 = 1 then diff ((n - 1) / 2) l
		else diff ((n - 2) / 2) r
      
let rec size = function
	| Empty -> 0
	| Fork(l,_,r) -> let m = size r in 2 * m + 1 + diff m l

	
(* Braun heap *)

type t =
	| Empty
	| Fork of t * int * t

let empty =
	Empty	

let singleton n =
	Fork(Empty,n,Empty)	
		
let rec add n = function
	| Empty -> singleton n
	| Fork(l,m,r) ->
		if n < m then Fork(add m r,n,l)
		else Fork(add n r,m,l)

let rec member = function
	| Empty -> invalid_arg "BraunHeap.member"
	| Fork(_,n,_) -> n

let rec replace n = function
	| Empty -> invalid_arg "BraunHeap.replace"
	| Fork((Fork(_,m,_) as l),_,Empty)
		when m < n ->
		Fork(replace n l,m,Empty)
	| Fork((Fork(_,na,_) as l),_,(Fork(_,nb,_) as r))
		when na < n || nb < n ->
		if na < nb
		then Fork(replace n l,na,r)
		else Fork(l,nb,replace n r)
	| Fork(l,_,r) -> Fork(l,n,r)

let rec remove = function
	| Empty -> invalid_arg "BraunHeap.remove"
	| Fork(t,_,Empty) -> t
	| Fork(Empty,_,_) -> assert false
	| Fork((Fork (_,na,_) as l),_,(Fork( _,nb,_) as r)) ->
		if na < nb
		then Fork(r,na,remove l)
		else Fork(replace na r,nb,remove l)

(* heap to sorted list *)
let rec to_list t =
	if is_empty t then []
	else member t :: to_list (remove t)
	

(* retrace heap *)

type t =
	| Empty
	| Fork of t * int * t

let empty =
	Empty

let singleton n =
	Fork(Empty,n,Empty)	
		
let rec add n t =
	match t with
	| Empty -> singleton n
	| Fork(_,m,_) when n > m -> Fork(t,n,Empty)
	| Fork(l,m,Empty) -> Fork(l,m,singleton n)
	| Fork(l,m,(Fork(_,k,_) as r)) when k > n ->
		Fork(l,m,add n r)
	| Fork(l,m,r) -> Fork(l,m,Fork(r,n,Empty))

let rec member = function
	| Empty -> invalid_arg "RetraceHeap.member"
	| Fork(_,n,_) -> n 

let rec remove = function
	| Empty -> invalid_arg "RetraceHeap.remove"
	| Fork(Empty,n,Empty) -> Empty
	| Fork(l,n,Empty) -> l
	| Fork(Empty,n,r) -> r
	| Fork((Fork(la,na,ra) as l),n,(Fork(lb,nb,rb) as r)) ->
		if nb > na then
			let ll = remove (Fork(l,n,lb))
			in  Fork(ll,nb,rb)
		else 
			let rr = remove (Fork(ra,n,r))
			in  Fork(la,na,rr)

(* iterative heap to sorted list *)
let rec to_list acc t =
	if is_empty t then acc
	else to_list (member t :: acc) (remove t)
let to_list =
	to_list []
	
	
(* pretty printers *)
let rec print_t = function
	| Empty -> print_string "[]"
	| Fork(Empty,n,Empty) -> print_char '[';print_int n;print_char ']'
	| Fork(l,n,Empty) -> print_char '[';print_t l;print_int n;print_string "[]]"
	| Fork(Empty,n,r) -> print_string "[[]";print_int n;print_t r;print_char ']'
	| Fork(l,n,r) ->
		print_char '[';print_t l;
		print_char ';';print_int n;print_char ';';print_t r;
		print_char ']';;
		
let print_text t =
	print_t (empty |> t);;
  