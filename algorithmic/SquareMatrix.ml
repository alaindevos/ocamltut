
module SquareMatrix
:
sig
   type 'a t
   val make : int -> 'a -> 'a t
   val add : ('a -> 'a -> 'a ) -> 'a t -> 'a t -> 'a t
end
=
struct
   type 'a t = 
      'a array array
   let make size x =
      Array.make_matrix size size x
   let add plus ma mb =
      Array.map2 (Array.map2 plus) ma mb
end
;;


module SquareMatrix
(M :
sig
   type t
   val size : int
   val plus : t -> t -> t
end
)
:
sig
   type t
   val make : M.t -> t
   val add : t -> t -> t
end
=
struct
   type t = 
      M.t array array
   let make x =
      Array.make_matrix M.size M.size x
   let add ma mb =
      Array.map2 (Array.map2 M.plus) ma mb
end
;;


module IntMatrix2x2 =
   SquareMatrix(struct type t=int let size=2 let plus=(+) end);;
module FloatMatrix3x3 =
   SquareMatrix(struct type t=float let size=3 let plus=(+.) end);;
module IntMatrixMatrix2x2 =
   SquareMatrix(struct type t=IntMatrix2x2.t let size=2 let plus=IntMatrix2x2.add end);;


(* Generative module-function *)

module Measure ()
:
sig
	type t
	val add: t -> t -> t
	(* other operators *)
end 
=
struct
	type t = float
	let add = (+.) 
	(* other operators *)
end
;; 

module Meter = Measure ();;
module Kilogram = Measure ();;
module Dollar = Measure ();;
module Euro = Measure ();;


(* Generative module-function *)

module Counter () =
struct
	let ticks = ref 0
	let tick () = incr ticks
	let count () = !ticks
end
;; 
