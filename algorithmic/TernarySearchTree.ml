

module TernarySearchTree
	:
sig
	type t
	val empty : t
	val add :    string -> t -> t
	val member : string -> t -> bool
	val remove : string -> t -> t
	val prefix : string -> t -> t
	val to_list : t -> string list
end
	=
struct (*...*)

	type t =
		| End
		| Path of char * t * t * t
		| Item of char * t * t * t
		
	let empty =
		End
		
	let rec member s i = function
		| End -> false
		| Path (c,lt,eq,gt) ->
			if s.[i] < c then member s i lt
			else if s.[i] > c then member s i gt 
			else member s (i+1) eq
		| Item (c,lt,eq,gt) ->
			if s.[i] < c then member s i lt
			else if s.[i] > c then member s i gt 
			else if i + 1 = String.length s then true
			else member s (i+1) eq

	let rec add s i = function
		| End -> 
			if i + 1 = String.length s then
				Item (s.[i],End,End,End)
			else 
				Path (s.[i],End,add s (i+1) End,End)
		| Path (c,lt,eq,gt) ->
			if s.[i] < c then Path (c,add s i lt,eq,gt) 
			else if s.[i] > c then Path (c,lt,eq,add s i gt)
			else if i + 1 = String.length s then Item (c,lt,eq,gt)
			else Path (c,lt,add s (i+1) eq,gt)
		| Item (c,lt,eq,gt) as t ->
			if s.[i] < c then Item (c,add s i lt,eq,gt) 
			else if s.[i] > c then Item (c,lt,eq,add s i gt)
			else if i + 1 = String.length s then t
			else Item (c,lt,add s (i+1) eq,gt)

	let rec remove s i = function
		| End -> 
			invalid_arg "TernarySearchTree.remove"
		| Path (c,lt,eq,gt) ->
			if s.[i] < c then Path (c,remove s i lt,eq,gt) 
			else if s.[i] > c then Path (c,lt,eq,remove s i gt)
			else Path (c,lt,remove s (i+1) eq,gt)
		| Item (c,lt,eq,gt) ->
			if s.[i] < c then Item (c,remove s i lt,eq,gt) 
			else if s.[i] > c then Item (c,lt,eq,remove s i gt)
			else if i + 1 = String.length s then Path (c,lt,eq,gt)
			else Item (c,lt,remove s (i+1) eq,gt)
	
	let rec prefix s i = function
		| End -> End
		| Path (c,lt,eq,gt) | Item (c,lt,eq,gt) ->
			if s.[i] < c then prefix s i lt
			else if s.[i] > c then prefix s i gt 
			else if i + 1 = String.length s then eq
			else prefix s (i+1) eq
	
	let member s = member s 0
	let add s = add s 0
	let remove s = remove s 0
	let prefix s = prefix s 0

	let append s c =
		s ^ String.make 1 c
	
	(* the recursive version *)
	let rec to_list s = function
		| End -> []
		| Path (c,lt,eq,gt) ->
			to_list s lt @ to_list (append s c) eq @ to_list s gt
		| Item (c,lt,eq,gt) -> let asc = append s c in 
			to_list s lt @ [asc] @ to_list asc eq @ to_list s gt
	
	let to_list = to_list ""
	
	(* the iterative version *)
	let rec to_list acc s = function
		| End -> acc
		| Path (c,lt,eq,gt) ->
			to_list (to_list (to_list acc s gt) (append s c) eq) s lt
		| Item (c,lt,eq,gt) -> let asc = append s c in 
			to_list (to_list (asc::to_list acc s gt) asc eq) s lt
	
	let to_list = to_list [] ""
	
end
		