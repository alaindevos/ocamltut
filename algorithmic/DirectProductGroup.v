
Module Operation.

  Section operations.
  
  Variable U : Set.
  
  Definition Operation := U -> U -> U.
  
  Variable op  : Operation.   (* the law     *)
  Variable e   : U.           (* the neutral *)
  Variable inv : U -> U.      (* the inverse *)
  
  Definition associative : Prop :=
    forall x y z: U, op x (op y z) = op (op x y) z.
  
  Definition left_neutral : Prop :=
    forall x: U, op e x = x.
  
  Definition right_neutral : Prop :=
    forall x: U, op x e = x.
  
  Definition neutral : Prop :=
    left_neutral /\ right_neutral.
  
  Definition left_inverse : Prop :=
    forall x: U, op (inv x) x = e.
  
  Definition right_inverse : Prop :=
    forall x: U, op x (inv x) = e.
  
  Definition symmetric_inverse : Prop :=
    left_inverse /\ right_inverse.
  
  End operations.

End Operation.


Module Type Monoid.

  Import Operation.

  Parameter U : Set. 
  Parameter law : Operation U.  
  Parameter zero : U.

  Axiom law_associative : associative U law.
  Axiom zero_neutral : neutral U law zero.

End Monoid.


Require Import Arith.Plus.


Module NatPlusZero : Monoid.

  Import Operation.

  Definition U := nat. 
  Definition law := plus.  
  Definition zero := O.

  Definition law_associative : associative U law.
    Proof.
    unfold associative. intros.
    unfold law. rewrite plus_assoc.
    reflexivity.
    Qed.
  Definition zero_neutral : neutral U law zero.
    Proof.
    unfold neutral. split.
    unfold left_neutral.
       intros. unfold law. unfold zero.
       rewrite plus_0_l. reflexivity.
    unfold right_neutral.
      intros. unfold law. unfold zero.
      rewrite plus_0_r. reflexivity.
    Qed.

End NatPlusZero.


Module Type Group.

  Include Monoid.
  Import Operation.

  Parameter inverse : U -> U.
  Axiom inverse_symmetric : symmetric_inverse U law zero inverse. 
 
End Group.


Module DirectProductGroup (H K : Group) : Group.

  Import Operation.
  
  Record product : Set := make {h : H.U; k : K.U}.
  
  Definition U := product. 
  Definition law x y := make (H.law (h x) (h y)) (K.law (k x) (k y)).
  Definition zero := make H.zero K.zero.
  Definition inverse x := make (H.inverse (h x)) (K.inverse (k x)).

  Definition law_associative : associative U law.
    Proof.
    unfold associative. intros.
    unfold law. simpl.
    rewrite H.law_associative.
    rewrite K.law_associative.
    reflexivity.
    Qed.
    
  Definition zero_neutral : neutral U law zero.
    Proof.
    unfold neutral. split. 
    unfold left_neutral.
      intros. unfold law. simpl.
      rewrite (proj1 H.zero_neutral).
      rewrite (proj1 K.zero_neutral).
      dependent inversion x. simpl. reflexivity.
    unfold right_neutral.
      intros. unfold law. simpl.
      rewrite (proj2 H.zero_neutral).
      rewrite (proj2 K.zero_neutral).
      dependent inversion x. simpl. reflexivity.
    Qed.
    
  Definition inverse_symmetric : symmetric_inverse U law zero inverse.
    Proof.
    unfold symmetric_inverse. split.
    unfold left_inverse.
      intros.
      unfold law. unfold inverse. simpl.
      rewrite (proj1 H.inverse_symmetric).
      rewrite (proj1 K.inverse_symmetric).
      reflexivity. 
    unfold right_inverse.
      intros.
      unfold law. unfold inverse. simpl.
      rewrite (proj2 H.inverse_symmetric).
      rewrite (proj2 K.inverse_symmetric).
      reflexivity. 
    Qed.

End DirectProductGroup.
