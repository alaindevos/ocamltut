
Reset Initial.

(* un arbre est soit vide soit une s�paration en deux *)
Inductive tree : Set :=
  | Empty: tree
  | Fork: tree -> nat -> tree -> tree.

Require Import Arith.Arith_base.

(* nombre de strahler *)
Fixpoint strahler t :=
  match t with
  | Empty => 0
  | Fork l n r =>
      let sl := strahler l in
      let sr := strahler r in
      if beq_nat sl sr then S sl else max sl sr    
  end.
   
(* recherche d'un entier n dans un arbre t *)
Fixpoint member_of n t :=
  match t with
  | Empty => false     (* absent *)
  | Fork l m r =>
      match n ?= m with
      | Eq => true     (* on l'a trouv� *)
      | Lt => member_of n l (* on cherche � gauche *)
      | Gt => member_of n r (* on cherche � droite *)
      end
  end.

(* insertion d'un entier n dans un arbre t *)
Fixpoint add n t :=
  match t with
  | Empty => Fork Empty n Empty  (* on cr�e un singleton *)
  | Fork l m r =>
      match n ?= m with
      | Eq => t                  (* d�j� pr�sent *)
      | Lt => Fork (add n l) m r (* on ins�re � gauche *)
      | Gt => Fork l m (add n r) (* on ins�re � droite *)
      end
  end.

(* extraction de l'intervalle [low,high] de l'arbre t *)
Fixpoint interval low high t :=
  match t with
  | Empty => Empty
  | Fork l n r =>
      match high ?= n with
      | Lt => interval low high l
      | Gt | Eq =>
          match low ?= n with
          | Gt => interval low high r
          | Lt | Eq => Fork (interval low high l) n (interval low high r)
          end
      end
  end.
  
(* c'est l� qu'est d�fini le type list *)
Require Import Lists.List.

(* conversion de l'arbre t en une liste tri�e *)
Fixpoint to_list acc t :=
  match t with
  | Empty => acc
  | Fork l n r => to_list (n::to_list acc r) l
  end.

(* trouve le plus petit �l�ment de l'arbre t *)
Fixpoint minimum acc t :=
  match t with
  | Empty => acc
  | Fork l n r => minimum n l  
  end.

(* supprime le plus petit �l�ment de l'arbre t *)
Fixpoint remove_minimum la na ra :=
  match la with
  | Empty => ra
  | Fork lb nb rb => Fork (remove_minimum lb nb rb) na ra
  end.

(* agglom�re les arbres ta et tb *)
(* les �l�ments de ta doivent �tre strictement inf�rieurs aux �l�ments de tb *)
Fixpoint concat ta tb :=
  match ta,tb with
  | _,Empty => ta
  | Empty,_ => tb
  | _,Fork lb nb rb =>
      Fork ta (minimum nb lb) (remove_minimum lb nb rb)
  end.

(* supprime l'�l�ment n de l'arbre t *)
Fixpoint remove n t :=
  match t with
  | Empty => Empty
  | Fork l m r =>
      match n ?= m with
      | Lt => Fork (remove n l) m r
      | Gt => Fork l m (remove n r)
      | Eq => concat l r
      end
  end.

(* filtre l'arbre t suivant le pr�dicat cond *)
Fixpoint filter (cond : nat -> bool) t := 
  match t with
  | Empty => Empty
  | Fork l n r =>
      if cond n then Fork (filter cond l) n (filter cond r)
      else concat (filter cond l) (filter cond r)	
  end.

(* nombres d'�l�ments de l'arbre t *)
Fixpoint cardinal t :=
  match t with
  | Empty => 0
  | Fork l _ r => cardinal l + 1 + cardinal r
  end.
	
(* c'est l� qu'est d�fini l'op�rateur && *)
Require Import Bool.Bool.

Require Import Coq.Program.Wf.

(* l'arbre ta est-il un sous-ensemble de l'arbre tb ? *)
Program Fixpoint subset (ta : tree) (tb : tree) {measure (cardinal ta + cardinal tb)} : bool :=
  match ta,tb with
  | Empty,_ => true
  | _,Empty => false
  | Fork la na ra,Fork lb nb rb => 
      match na ?= nb with
      | Lt => subset (Fork la na Empty) lb && subset ra tb
      | Gt => subset (Fork Empty na ra) rb && subset la tb
      | Eq => subset la lb && subset ra rb
      end
  end.
Obligation 1.
Proof.
  simpl.
  do 4 rewrite <- Nat.add_assoc.
  do 2 apply plus_lt_compat_l.
  rewrite Nat.add_comm.
  rewrite Nat.add_assoc.
  rewrite (Nat.add_comm (cardinal ra)).
  do 2 rewrite <- Nat.add_assoc.
  apply plus_lt_compat_l.
  apply Nat.lt_lt_add_r.
  auto.
Qed.
Obligation 2.
Proof.
  simpl.
  rewrite (Nat.add_comm (cardinal la + 1)).
  do 2 rewrite <- Nat.add_assoc.
  apply plus_lt_compat_l.
  rewrite Nat.add_assoc.
  rewrite (Nat.add_comm (cardinal la + 1)).
  do 3 rewrite <- Nat.add_assoc.
  do 2 apply plus_lt_compat_l.
  apply Nat.lt_add_pos_r.
  rewrite Nat.add_1_r.
  apply Nat.lt_0_succ.
Qed.
Obligation 3.
Proof.
  simpl.
  rewrite (Nat.add_comm (_) 1); rewrite <- Nat.add_shuffle1.
  rewrite Nat.add_shuffle0; rewrite (Nat.add_assoc 1).
  rewrite <- (Nat.add_assoc _ (cardinal la)).
  rewrite (Nat.add_comm (cardinal la)).
  do 4 rewrite <- Nat.add_assoc; do 2 apply plus_lt_compat_l.
  apply Nat.lt_add_pos_r.
  rewrite Nat.add_1_r.
  apply (Nat.lt_lt_add_l 0 (S (cardinal lb)) (cardinal la)).
  apply Nat.lt_0_succ.
Qed.
Obligation 4.
Proof.
  simpl.
  rewrite Nat.add_shuffle0. 
  rewrite <- (Nat.add_assoc _ (cardinal ra)). 
  rewrite (Nat.add_comm (cardinal ra)); rewrite (Nat.add_assoc (cardinal _ + _)). 
  rewrite <- (Nat.add_assoc _ 1); rewrite (Nat.add_comm 1 (_ + _)).
  do 5 rewrite <- Nat.add_assoc.
  do 3 apply plus_lt_compat_l.
  apply Nat.lt_add_pos_r.
  rewrite Nat.add_1_l.  
  apply Nat.lt_0_succ.
Qed.
Obligation 5.
Proof.
  simpl.
  rewrite Nat.add_shuffle0; rewrite <- (Nat.add_assoc _ 1).
  rewrite (Nat.add_comm 1 _); do 4 rewrite <- Nat.add_assoc.
  apply plus_lt_compat_l.
  apply Nat.lt_add_pos_r.
  rewrite Nat.add_1_l.  
  apply Nat.lt_0_succ.
Qed.
Obligation 6.
Proof.
  simpl.
  rewrite (Nat.add_comm (_ + 1));  rewrite Nat.add_shuffle2.
  apply Nat.lt_add_pos_r; rewrite (Nat.add_comm _ 1).
  rewrite Nat.add_1_l; apply Nat.lt_0_succ.
Qed.

Require Import Lia.

(* l'arbre ta est-il un sous-ensemble de l'arbre tb ? *)
Program Fixpoint subset (ta : tree) (tb : tree) {measure (cardinal ta + cardinal tb)} : bool :=
  match ta,tb with
  | Empty,_ => true
  | _,Empty => false
  | Fork la na ra,Fork lb nb rb => 
      match na ?= nb with
      | Lt => subset (Fork la na Empty) lb && subset ra tb
      | Gt => subset (Fork Empty na ra) rb && subset la tb
      | Eq => subset la lb && subset ra rb
      end
  end.
Next Obligation. Proof. simpl; lia. Qed.
Next Obligation. Proof. simpl; lia. Qed.
Next Obligation. Proof. simpl; lia. Qed.
Next Obligation. Proof. simpl; lia. Qed.
Next Obligation. Proof. simpl; lia. Qed.
Next Obligation. Proof. simpl; lia. Qed.

(* inclusion r�ciproque *)
Definition equal ta tb :=
  subset ta tb && subset tb ta.  
  
(* d�coupe t en deux parties inf�rieures et sup�rieures � n *)
Fixpoint split n t :=
  match t with
  | Empty => (Empty,false,Empty)
  | Fork l m r =>
      match n ?= m with
      | Lt => let '(la,present,ra) := split n l in (la,present,Fork ra m r)
      | Gt => let '(lb,present,rb) := split n r in (Fork l m lb,present,rb)
      | Eq => (l,true,r)
      end
  end.

(* fusionne les arbres ta et tb *)
Fixpoint union ta tb :=
  match ta,tb with
  | _,Empty => ta
  | Empty,_ => tb
  | Fork la na ra,_ => 
      let '(lb,_,rb) := split na tb
      in Fork (union la lb) na (union ra rb)
  end.

Fixpoint intersection ta tb :=
  match ta,tb with
  | _,Empty | Empty,_ => Empty
  | Fork la na ra,_ => 
      let '(lb,present,rb) := split na tb in
      if present then Fork (intersection la lb) na (intersection ra rb)
      else concat (intersection la lb) (intersection ra rb)
  end.

Fixpoint disjoint ta tb :=
  match ta,tb with
  | _,Empty | Empty,_ => true
  | Fork la na ra,_ => 
      let '(lb,present,rb) := split na tb in
      if present then false
      else disjoint la lb && disjoint ra rb
  end.

Fixpoint difference ta tb :=
  match ta,tb with
  | _,Empty => ta
  | Empty,_ => Empty
  | Fork la na ra,_ => 
      let '(lb,present,rb) := split na tb in
      if present then concat (difference la lb) (difference ra rb)
      else Fork (difference la lb) na (difference ra rb)
  end.

Inductive member : nat -> tree -> Prop :=
  | root_member :
      forall l n r,
      member n (Fork l n r)
  | left_member :
      forall m n l r,
      member n l ->
      member n (Fork l m r)
  | right_member :
      forall m n l r,
      member n r ->
      member n (Fork l m r).

Theorem add_main_property :
  forall n t, member n (add n t).
Proof.
  intros n t. induction t.
  (* Empty *)
  unfold add.
    apply root_member.
  (* Fork *)
  unfold add. remember (n ?= n0) as cmp.
  destruct cmp.
    (* Eq *)
    symmetry in Heqcmp.
    apply (nat_compare_eq n n0) in Heqcmp.
    subst n0. apply root_member.
    (* Lt *)
    apply left_member. exact IHt1.
    (* Gt *)
    apply right_member. exact IHt2.
Qed.

Theorem add_is_conservative :
  forall m n t, member m t -> member m (add n t).
Proof.
  intros m n t H. induction t. 
  (* Empty *)
  unfold add.
    apply left_member. exact H.
  (* Fork *)
  remember (Fork t1 n0 t2) as node.
  destruct H.
  (* root_member *)
  unfold add. destruct (n ?= n1).
    apply root_member.
    apply root_member.
    apply root_member.
  (* left_member *)
  inversion Heqnode. rewrite H1 in H.
  unfold add. destruct (n ?= n0).
    apply left_member. exact H.
    apply IHt1 in H. apply left_member. exact H.
    apply left_member. exact H.
  (* right_member *)
  inversion Heqnode. rewrite H3 in H.
  unfold add. destruct (n ?= n0).
    apply right_member. exact H.
    apply right_member. exact H.
    apply IHt2 in H. apply right_member. exact H.
Qed.

Inductive tree_less : tree -> nat -> Prop :=
  | empty_tree_less :
      forall b, tree_less Empty b
  | fork_tree_less :
      forall l m r b,
      tree_less l b -> tree_less r b -> m < b ->
      tree_less (Fork l m r) b.
 
Inductive tree_more : tree -> nat -> Prop :=
  | empty_tree_more :
      forall a, tree_more Empty a
  | fork_tree_more :
      forall l m r a,
      tree_more l a -> tree_more r a -> m > a ->
      tree_more (Fork l m r) a.
 
Inductive tree_ordered : tree -> Prop :=
  | empty_tree_ordered :
      tree_ordered Empty
  | fork_tree_ordered :
      forall l m r,
      tree_ordered l -> tree_ordered r ->
      tree_less l m -> tree_more r m ->
      tree_ordered (Fork l m r).

Fact tree_less_upper_bound:
  forall t n, tree_less t n <-> forall m, member m t -> m < n.
Proof.
  split.
  (* -> *)
  intros Hless m Hm. induction t.
  inversion Hm.
  inversion Hm; inversion Hless; subst; auto.
  (* <- *)
  intros H. induction t.
    constructor.
    constructor.
      apply IHt1. intros m Hm. apply H. apply left_member. assumption.
      apply IHt2. intros m Hm. apply H. apply right_member. assumption.
      apply H. apply root_member.
Qed.

Fact tree_more_lower_bound:
  forall t n, tree_more t n <-> forall m, member m t -> m > n.
Proof.
  split.
  (* -> *)
  intros Hmore m Hm. induction t.
  inversion Hm.
  inversion Hm; inversion Hmore; subst; auto.
  (* <- *)
  intros H. induction t.
    constructor.
    constructor.
      apply IHt1. intros m Hm. apply H. apply left_member. assumption.
      apply IHt2. intros m Hm. apply H. apply right_member. assumption.
      apply H. apply root_member.
Qed.

(* Require Import Arith.Lt Arith.Gt. *)

Lemma member_left:
  forall l m r n, tree_ordered (Fork l m r) -> member n (Fork l m r) -> n < m -> member n l.
Proof.
  intros l m r n Hord Hmem Hlt.
  inversion Hord; subst; clear Hord.
  inversion Hmem; subst; clear Hmem.
    contradict Hlt. apply lt_irrefl.
    assumption.
    apply tree_more_lower_bound with (m:=n) in H5.
      contradict H5. apply gt_asym. assumption.
      assumption.
Qed.
 
Lemma member_right:
  forall l m r n, tree_ordered (Fork l m r) -> member n (Fork l m r) -> n > m -> member n r.
Proof.
  intros l m r n Hord Hmem Hlt.
  inversion Hord; subst; clear Hord.
  inversion Hmem; subst; clear Hmem.
    contradict Hlt. apply lt_irrefl.
    apply tree_less_upper_bound with (m:=n) in H4.
      contradict H4. apply lt_asym. assumption. assumption.
    assumption.
Qed.

Lemma add_preserves_less:
  forall t n m, tree_less t n -> m < n -> tree_less (add m t) n.
Proof.
  induction t; intros; inversion H; subst; clear H.
  repeat constructor. assumption.
  simpl. destruct (m ?= n); constructor; auto.
Qed.
 
Lemma add_preserves_more:
  forall t n m, tree_more t n -> m > n -> tree_more (add m t) n.
Proof.
  induction t; intros; inversion H; subst; clear H.
  repeat constructor. assumption.
  simpl. destruct (m ?= n); constructor; auto.
Qed.

Theorem add_preserves_order :
  forall t, tree_ordered t ->
  forall n, tree_ordered (add n t).
Proof.
  induction t; intros.
  (* Empty *)
  simpl. repeat constructor.
  (* Fork *)
  simpl. remember (n0 ?= n) as cmp. symmetry in Heqcmp.
  destruct cmp.
    assumption.
    inversion H; subst; clear H. constructor; auto.
      apply nat_compare_lt in Heqcmp. apply add_preserves_less; assumption.
    inversion H; subst; clear H. constructor; auto.
      apply nat_compare_gt in Heqcmp. apply add_preserves_more; assumption.
Qed.

Theorem member_of_iff_member :
  forall t, tree_ordered t ->
  forall n, member_of n t = true <-> member n t.
Proof.
  intros t ord n.
  split; intro H.
    (* -> *)
    induction t.
      (* Empty *)
      inversion H.
      (* Fork *)
      inversion_clear ord.
      simpl in H. remember (n ?= n0) as cmp.
      symmetry in Heqcmp. destruct cmp.
        (* n = n0 *)
        apply nat_compare_eq in Heqcmp. subst.
        apply root_member.
        (* n < n0 *)
        apply left_member. apply IHt1.
          assumption. assumption.
        (* n > n0 *)
        apply right_member. apply IHt2.
          assumption. assumption.
    (* <- *)
    induction t.
      (* Empty *)
      inversion H.
      (* Fork *)
      simpl. remember (n ?= n0) as cmp.
      symmetry in Heqcmp. destruct cmp.
        (* n = m *)
        reflexivity.
        (* n < m *)
        apply IHt1.
          inversion ord. assumption.
          apply nat_compare_lt in Heqcmp.
            apply member_left with (n:=n) in ord.
              assumption. assumption. assumption.
        (* n > m *)
        apply IHt2.
          inversion ord. assumption.
          apply nat_compare_gt in Heqcmp.
            apply member_right with (n:=n) in ord.
              assumption. assumption. assumption.
Qed.

(* specification des op�rations ensemblistes *)

Theorem interval_main_property :
  forall low high n t, member n (interval low high t) <-> member n t /\ low <= n <= high.
Proof.
Admitted.

Theorem remove_main_property :
  forall n t, ~ member n (remove n t).
Proof.
Admitted.

Theorem remove_is_conservative :
  forall m n t, member m t /\ m <> n -> member m (remove n t).
Proof.
Admitted.

Theorem filter_main_property :
  forall n t cond, member n (filter cond t) <-> member n t /\ cond n.
Proof.
Admitted.

Theorem union_main_property :
  forall n ta tb, member n ta \/ member n tb <-> member n (union ta tb).
Proof.
Admitted.

Theorem intersection_main_property :
  forall n ta tb, member n ta /\ member n tb <-> member n (intersection ta tb).
Proof.
Admitted.

Theorem subset_main_property :
  forall n ta tb, (member n ta -> member n tb) <-> subset ta tb.
Proof.
Admitted.

Theorem disjoint_main_property :
  forall n ta tb, (member n ta -> ~ member n tb) <-> disjoint ta tb = true.
Proof.
Admitted.

Theorem difference_main_property :
  forall n ta tb, (member n ta /\ ~ member n tb) <-> member n (difference ta tb).
Proof.
Admitted.

Theorem interval_preserves_order :
  forall t, tree_ordered t ->
  forall low high, tree_ordered (interval low high t).
Proof.
Admitted.

Theorem remove_preserves_order :
  forall t, tree_ordered t ->
  forall n, tree_ordered (remove n t).
Proof.
Admitted.

Theorem filter_preserves_order :
  forall t, tree_ordered t ->
  forall cond, tree_ordered (filter cond t).
Proof.
Admitted.

Theorem union_preserves_order :
  forall ta tb, tree_ordered ta /\ tree_ordered tb -> tree_ordered (union ta tb).
Proof.
Admitted.

Theorem intersection_preserves_order :
  forall ta tb, tree_ordered ta /\ tree_ordered tb -> tree_ordered (intersection ta tb).
Proof.
Admitted.

Theorem difference_preserves_order :
  forall ta tb, tree_ordered ta /\ tree_ordered tb -> tree_ordered (difference ta tb).
Proof.
Admitted.

(* implantation d'une pile de Braun *)

(* insertion d'un entier n dans un arbre de Braun t *)
Fixpoint add n t :=
  match t with
  | Empty => Fork Empty n Empty
  | Fork l m r => Fork (add m r) n l
  end.
  
