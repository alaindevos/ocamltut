
module type Type =
sig
	type t = 
	| X          (* the x variable *)
	| R of float (* a real number  *)
	| Sin of t
	| Cos of t
	| Tan of t
	| Log of t
	| Exp of t
	| Power of t * float
	| Add of t * t
	| Mul of t * t
	val derive : t -> t
end

module Data
:
Type
=
struct

	type t = 
	| X          (* the x variable *)
	| R of float (* a real number  *)
	| Sin of t
	| Cos of t
	| Tan of t
	| Log of t
	| Exp of t
	| Power of t * float
	| Add of t * t
	| Mul of t * t

let factor = function
  | R(1.),u -> u
  | u,v -> Mul(u,v)

let product = function
  | R(a),Mul(R(b),u) -> factor(R(a*.b),u)
  | Mul(R(a),u),R(b) -> factor(R(a*.b),u)
  | Mul(R(a),u),Mul(R(b),v) -> factor(R(a*.b),Mul(u,v))
  | u,Mul(R(k),v) -> Mul(R(k),Mul(u,v))
  | Mul(R(k),u),v -> Mul(R(k),Mul(u,v))
  | u,v -> factor(u,v)  

let rec derive = function
	| X -> R(1.)
	| R(k) -> R(0.)
	| Add(u,R(k)) -> derive(u)
	| Add(u,v)	  -> Add(derive(u),derive(v))
	| Mul(R(k),X) -> R(k)
	| Mul(R(k),u) -> product(R(k),derive(u))
	| Mul(u,v)	  -> Add(product(derive(u),v),product(u,derive(v)))
	| Sin(u) -> product(derive(u),Cos(u))
	| Cos(u) -> product(R(-1.),product(derive(u),Sin(u)))
	| Tan(u) -> product(derive(u),Power(Cos(u),-2.))
	| Log(u) -> product(derive(u),Power(u,-1.))
	| Exp(u) -> product(derive(u),Exp(u))
	| Power(u,2.) -> product(R(2.),product(derive(u),u))
	| Power(u,a)  -> product(R(a),product(derive(u),Power(u,a-.1.)))
end
