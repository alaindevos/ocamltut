
(* the brute-force functional style *)

let rec distribute a l =
	match l with 
	| []   -> [[a]]
	| h::t -> (a::l) :: List.map (fun x -> h::x) (distribute a t)
  
let rec permute = function
	| []   -> [[]]
	| h::t -> List.flatten (List.map (distribute h) (permute t))

let rec one_safe low high = function
	| []   -> true
	| h::t -> h <> low && h <> high && one_safe (low - 1) (high + 1) t

let rec all_safe = function
	| []   -> true
	| h::t -> one_safe (h - 1) (h + 1) t && all_safe t  

let _ =
	List.filter all_safe (permute [1;2;3;4;5;6;7;8]);;
	
	
(* the backtrack OOP style *)

let null_queen =
	object
		method first = false 
		method next = false 
		method check (x:int) (y:int) = false 
		method print = print_newline ()
	end

class queen column (neighbor:queen) =
	object (self)
		val mutable row = 1
		method check r c =
			let diff = c - column in
			(row = r) ||
			(row + diff = r) || (row - diff = r) ||
			neighbor#check r c
		method private test =
			try
				while neighbor#check row column do
					if not self#advance then raise Exit
				done;
				true
			with
			| Exit -> false
		method first =
			ignore(neighbor#first);
			row <- 1;
			self#test
		method next =
			self#advance && self#test
		method private advance =
			if row = 8 then
				(if neighbor#next then (row <- 1; true) else false)
			else (row <- row + 1; true)
		method  print : unit =
			print_int row; print_char ' '; neighbor#print
	end

let () =
	let last = ref null_queen in
	for i = 1 to 8 do
		last := new queen i !last
 	done;
	if !last#first then begin  
		!last#print;
		while !last#next do
			!last#print
		done
	end else
		print_string "Zero solution."
	