
(* categorical functor *)
module type Functor =
sig
	type 'a t
	val map : ('a -> 'b) -> 'a t -> 'b t
end

(* minimalist intuition about a polymorph data collection *)
module type DataCollection =
sig
	include Functor
	val size : 'a t -> int 
	type index
	val member : index -> 'a t -> 'a
end

(* Stdlib.List *)
module StdList
:
sig
	include DataCollection
	with type 'a t = 'a list 
	with type index = int 
end
=
struct
	type 'a t =
		'a list
	let map =
		List.map
	let size =
		List.length
	type index =
		int
	let member n l =
		List.nth l n
end

(* Stdlib.Array *)
module StdArray
:
sig
	include DataCollection
	with type 'a t = 'a array
	with type index = int 
end
=
struct
	type 'a t =
		'a array
	let map =
		Array.map
	let size =
		Array.length
	type index =
		int
	let member n a =
		Array.get a n
end

(* type PairIndex.index *)
module PairIndex
=
struct
	type index = Fst | Snd
end

(* a pair of values *)
module Pair
:
sig
	include DataCollection
	with type 'a t = 'a * 'a
	with type index = PairIndex.index
end
=
struct
	type 'a t =
		'a * 'a
	let map f (x,y) =
		(f x,f y)
	let size t =
		2
	type index =
		PairIndex.index
	let member i (x,y) =
		let open PairIndex in
		match i with
		| Fst -> x
		| Snd -> y
end


(* Braun min-heap *)
module BraunHeap
:
sig
	include DataCollection
	with type index = unit
	val empty : 'a t
	val add : 'a -> 'a t -> 'a t 
	val replace : 'a -> 'a t -> 'a t
	val remove : 'a t -> 'a t 
end
=
struct
	type 'a t =
		| Empty
		| Fork of 'a t * 'a * 'a t
	let rec map f = function
		| Empty -> Empty
		| Fork(l,n,r) -> Fork(map f l,f n,map f r)
	let rec diff n = function
		| Empty -> if n = 0 then 0 else assert false
		| Fork(l,_,r) ->
			if n = 0 then 1
			else if n mod 2 = 1 then diff ((n - 1) / 2) l
			else diff ((n - 2) / 2) r
	let rec size = function
		| Empty -> 0
		| Fork(l,_,r) -> let m = size r in 2 * m + 1 + diff m l
	type index =
		unit
	let member () = function
		| Empty -> invalid_arg "BraunHeap.member"
		| Fork(_,n,_) -> n
	let empty =
		Empty
	let rec add n = function
		| Empty -> Fork(Empty,n,Empty)
		| Fork(l,m,r) ->
			if n < m then Fork(add m r,n,l)
			else Fork(add n r,m,l)
	let rec replace n = function
		| Empty -> invalid_arg "BraunHeap.replace"
		| Fork((Fork(_,m,_) as l),_,Empty)
			when m < n ->
			Fork(replace n l,m,Empty)
		| Fork((Fork(_,na,_) as l),_,(Fork(_,nb,_) as r))
			when na < n || nb < n ->
			if na < nb
			then Fork(replace n l,na,r)
			else Fork(l,nb,replace n r)
		| Fork(l,_,r) -> Fork(l,n,r)
	let rec remove = function
		| Empty -> invalid_arg "BraunHeap.remove"
		| Fork(t,_,Empty) -> t
		| Fork(Empty,_,_) -> assert false
		| Fork((Fork (_,na,_) as l),_,(Fork( _,nb,_) as r)) ->
			if na < nb
			then Fork(r,na,remove l)
			else Fork(replace na r,nb,remove l)
end


(* type 'a NestT.t *)
module NestT
=
struct
	type 'a t =
		| Nil
		| Cons of 'a * ('a * 'a) t
end

(* a nested datatype from the paper "Nested Datatypes"
 * by Richard Bird and Lambert Meertens 1998
 *)
module Nest
:
sig
	include DataCollection
	with type 'a t = 'a NestT.t
	with type index = int
end
=
struct
	type 'a t =
		'a NestT.t
	let rec map : 'a 'b . ('a -> 'b) -> 'a t -> 'b t = 
		fun f -> function
		| Nil -> Nil
		| Cons(h,t) -> Cons(f h,map (Pair.map f) t)
	let rec size : 'a . 'a t -> int = function
		| Nil -> 0
		| Cons(_,t) -> 1 + 2 * size t
	type index =
		int
	let rec member : 'a . index -> 'a t -> 'a =
		fun n -> function
		| Nil -> failwith "Nest.member"
		| Cons(h,t) ->
				if n=0 then h else
				let x,y = member (n/2) t in
				if n mod 2 = 0 then x else y
end

(*
(* another nested datatype from the paper "Nested Datatypes"
 * by Richard Bird and Lambert Meertens 1998
 *)
module Bush
:
sig
	include DataCollection
	with type 'a t =
		| Nil
		| Cons of 'a * 'a t t
	with type index = ...
end
=
struct
	type 'a t =
		'a BushT.t
	let rec map : 'a 'b. ('a -> 'b) -> 'a t -> 'b t =
		fun f -> function
		| Nil -> Nil
		| Cons(h, t) -> Cons(f h,map (map f) t)
	...
end
*)
