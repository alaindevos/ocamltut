let _ =
    (*tuples*)
    let p = (1,"Hello") in
    let (x,y)=p in
    let fst (x,_)=x in
    print_int (fst p);
    let make_coord = fun x y -> (x,y) in
    let z= make_coord 1 2 in
    print_int (fst z);
    (*Lists*)
    let lis="Hello"::"World"::[] in
    let rec sum x = 
        match x with
        | [] -> 0
        | h::t -> h + (sum t) in
    let rec isin x lis =
        match lis with
        | [] -> false
        | h::t -> if h=x then true
        else 
            (isin x t) in
    let rec map f x =
        match x with
        | [] -> []
        | h::t -> (f h)::(map f t) in
    (* associative list*)
    let rec assoc key lis=
        match lis with
        | (key2,value2)::t -> if key2=key then value2 
        else 
            assoc key t
        | [] -> raise Not_found in
    (*Unions*)
    ()




