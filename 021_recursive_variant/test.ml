(* Recursive variant *****************************************************)
type intlist = Nil | Cons of int * intlist
let _=
    let rec sum (il:intlist):int=
        match il with
            | Nil -> 0
            | Cons (h,t) -> h + sum t in 
    let l0:intlist=Nil in
    let l1:intlist=Cons(1,l0) in
    let l2:intlist=Cons(2,l1) in
    let mysum:int=sum l2 in
    print_int mysum
