(* Read a file line by line *************************)
exception Dont_get_here;;

let fprint_file channel =
    let t = ref 0 in
    let rec fprint_line channel =
        let ilc = input_line channel in
        match ilc with
        | line ->
            incr t;
            Printf.printf "%d:" !t;
            print_endline line;
            fprint_line channel
        | exception End_of_file -> ()
    in
    fprint_line channel
in

let fprint_file (name : string) =
    let channel = open_in name in
    try
      let () = fprint_file channel in
      let () = raise Dont_get_here in
      let () = close_in channel in
      ()
    with e ->
      let () = close_in_noerr channel in
      (* Debug message *)
      print_string (Printexc.to_string e)
in

let () = fprint_file "file.txt" in
()
