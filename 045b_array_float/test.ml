(* array of float ***************************************)
type vec=float array

let vecprint1 v=
    for i=0 to Array.length v-1 do 
        let ()=print_float v.(i) in 
        let ()=print_newline () in 
        ()
    done;;

let vecprint2 v=
    let printel n=
        let ()=print_float n in 
        let ()=print_newline () in
        () in 
    Array.iter printel v ;;

let vecprint3 v=
    Array.iter (Printf.printf "%F\n") v;;


let v=[|1.;0.|];;
vecprint1 v;;
vecprint2 v;;
vecprint3 v;;

