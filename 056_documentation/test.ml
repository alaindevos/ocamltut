(* Documentation *********************************************************)
(*ocamlopt -o test str.cmxa   test.ml *)
(*ocamldoc -html -d html      test.ml *)
(*ocamldoc -man  -d man       test.ml *)

(** Example : Comment associated with whole module *)

(** Comment for type mytype *)
type weather =
    | Rain
    | Sun

(** Comment for function add *)
let add x y = x+y;;
